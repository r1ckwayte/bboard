# API


## Auth

`api/token/`


* [ ] analytics
* [ ] area
* [ ] main
    * [ ] products (GET all limit 10)
    * [ ] products/create
    * [ ] products/update/id
    * [ ] products/delete/id
* [ ] cms
    * [ ] about
    * [ ] help
    * [ ] license
    * [ ] feedback
* [ ] cart
* [ ] catalog?shop
* [ ] users
    * [ ] login
    * [ ] logout
    * [ ] registration
    * [ ] registration/validate?
    * [ ] registration/create
    * [x] accounts/ all & create
    * [x] account/<int:pk>/ update/delete
    * [ ] account/settings

* **SESSION_COOKIE_AGE** – время жизни сессии на основе куков в секундах. Значение по умолчанию – 1209600 (2 недели);
* **SESSION_COOKIE_DOMAIN** – домен для сессий на основе куков. Установите эту
настройку равной домену вашего сайта или None, чтобы избежать угрозы
подмены куков
* **SESSION_COOKIE_DOMAIN** – булево значение, говорящее о том, может ли сессия на основе куков быть задана через HTTP- и HTTPS-соединения или
только через HTTPS
* **SESSION_EXPIRE_AT_BROWSER_CLOSE** – время жизни сессии на основе куков после
закрытия браузера
* **SESSION_SAVE_EVERY_REQUEST** – булево значение. Если оно равно True, сессия
будет сохраняться в базу данных при каждом запросе. При этом время
окончания ее действия будет автоматически обновляться.
Все доступные настройки подсистемы сессий вы можете найти