# Filter

* https://www.caktusgroup.com/blog/2018/10/18/filtering-and-pagination-django/
* https://django-filter.readthedocs.io/en/stable/ref/filterset.html
* https://django-shop.readthedocs.io/en/latest/reference/filters.html
* https://codepen.io/ranwise/pen/ZEOYErb
 

* https://design-glory.com/3679/9-sovetov-po-apgreidu-filtrov

* [Как магазины хранят фильтры в URL? Плюс разбор и процентная нотация!](https://www.youtube.com/watch?v=pcPiI2-wmHE)

* django filter products

`Item.objects.filter(price__gte=536697, price__lte=536697).values('name', 'price')`


* https://spb.cian.ru/kupit-kvartiru-sankt-peterburg-bolshaya-pushkarskaya-ulica-024652/

## Фильтры

- фильтр по свойствам варианта продуктов
    - категория
    - бренд
    - цена
    - наличие\доступность

- фильтр по характеристикам
    - Цвет
    - Размер

* поле поиска
* поле выбора города (dropdown)
    * диапазон область км  (dropdown)
        * `+ 0 km`
        * `+ 2 km`
        * `+ 5 km`
        * `+ 10 km`
        * `+ 15 km`
        * `+ 30 km`
        * `+ 50 km`
        * `+ 75 km`
        * `+ 100 km`
* checkbox inline
    * Искать в заголовке и описании
    * Только с фото
    * Товары с доставкой
* category main\sub\ type category (dropdown)
* район (dropdown)
* Состояние (dropdown)
    * Все
    * Б/у
    * Новый
* цена (зависит от валюты?)
    * от (dropdown)
        * 100 грн.
        * 500 грн.
        * 1 000 грн.
        * 1 500 грн.
        * 2 000 грн.
        * 2 500 грн.
        * 3 000 грн.
        * 4 000 грн.
        * 5 000 грн.
        * Отдам даром
        * Обмен
    * до (dropdown)
        * 100 грн.
        * 500 грн.
        * 1 000 грн.
        * 1 500 грн.
        * 2 000 грн.
        * 2 500 грн.
        * 3 000 грн.
        * 4 000 грн.
        * 5 000 грн.
* Валюта: грн. $ € (line tabs)
* Сортировка ( line tabs)
    * Самые новые
    * Самые дешевые
    * Самые дороги
* Тип объявления (line tabs)
    * Все
    * Частное
    * Бизнес (shop)