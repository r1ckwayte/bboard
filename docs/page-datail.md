# Detail Page

* add [Крутой шаринг страниц в соцсети с помощью Open Graph](https://habr.com/ru/post/278459/)
* [Что такое разметка Open Graph и как ее внедрить без программиста](https://habr.com/ru/company/click/blog/492258/)

Для Фейсбука, Вконтакта, Одноклассников и Гуглплюса:

```html
<meta property="og:type" content="website">
<meta property="og:site_name" content="Название сайта">
<meta property="og:title" content="Заголовок">
<meta property="og:description" content="Описание.">
<meta property="og:url" content="http://example.com/page.html">
<meta property="og:locale" content="ru_RU">
<meta property="og:image" content="http://example.com/img.jpg">
<meta property="og:image:width" content="968">
<meta property="og:image:height" content="504">

```


Для Твиттера и Вконтакта (ВКонтакт выберет для заголовка тот title, который в коде будет расположен ниже):

```html
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="Заголовок">
<meta name="twitter:description" content="Описание.">
<meta name="twitter:image:src" content="http://example.com/img.jpg">
<meta name="twitter:url" content="http://example.com/page.html">
<meta name="twitter:domain" content="example.com">
<meta name="twitter:site" content="@">
<meta name="twitter:creator" content="@...">
```

Да, 968×504 пискеля это меньше минимально рекомендованных Фейсбуком 1200×630. Зато при таком размере и ратио картинку нигде не кропят, и выглядит она отлично.

## Images optimize

* https://css-tricks.com/beyond-media-queries-using-newer-html-css-features-for-responsive-designs/