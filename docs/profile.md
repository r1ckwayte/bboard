# Profile

* https://www.devhandbook.com/django/user-profile/

django user profile page

## Registration

* https://dev.to/coderasha/create-advanced-user-sign-up-view-in-django-step-by-step-k9m

## Login

## User info for admin

```python

from django.db.models import (
    Count,
    Sum,
    Case,
    When,
    Value,
    IntegerField,
    F,
)
from modules.users.models import *

Account.objects.aggregate(
    total_users=Count('id'),
    total_active_users=Count('id', filter=F('is_active')),
)

# {'total_users': 63, 'total_active_users': 63}

Account.objects.aggregate(
    total_users=Count('id'),
    total_active_users=Sum(Case(
        When(is_active=True, then=Value(1)),
        default=Value(0),
        output_field=IntegerField(),
    )),
)

```