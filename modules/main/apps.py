from django.apps import AppConfig


class MainConfig(AppConfig):
    name = 'modules.main'
    verbose_name = 'Торговая площадка'
