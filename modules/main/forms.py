from django import forms
from mptt.forms import TreeNodeChoiceField

from modules.catalog.models import Category
from modules.main.fields import GroupedModelChoiceField
from modules.main.models import Item


class ItemFilterForm(forms.Form):
    # category = forms.ModelChoiceField(
    #     label="Category",
    #     required=False,
    #     queryset=Category.objects.filter()
    # )
    price__gte = forms.FloatField(
        min_value=0,
        required=False,
        label='Цена от'
    )
    price__lte = forms.FloatField(
        min_value=0,
        required=False,
        label='Цена до'
    )

    TYPE_ITEM = (
        (None, "All"),
        (1, "Коммерческое"),
        (0, 'Частное'),
    )

    commercial = forms.ChoiceField(
        required=False,
        choices=TYPE_ITEM,
    )


class SearchForm(forms.Form):
    keyword = forms.CharField(required=False, max_length=20, label='')


class AddItemForm(forms.ModelForm):
    # category = GroupedModelChoiceField(
    #     queryset=Category.objects.exclude(parent=None),
    #     choices_groupby='parent'
    # )
    category = TreeNodeChoiceField(queryset=Category.objects.all())

    # commercial = forms.BooleanField(widget=forms.HiddenInput(), initial=0)

    class Meta:
        model = Item
        fields = ('category', 'name', 'description', 'city_bb', 'price', 'currency',)


class EditItemForm(forms.ModelForm):
    # category = GroupedModelChoiceField(
    #     queryset=Category.objects.exclude(parent=None),
    #     choices_groupby='parent'
    # )
    category = TreeNodeChoiceField(queryset=Category.objects.all())

    class Meta:
        model = Item
        fields = (
            'category',
            'name',
            'description',
            'price',
            'currency',
            'phone',
            'city_bb',
            'is_active'
        )
