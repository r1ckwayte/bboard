from django.core.management.base import BaseCommand
from modules.area.models import City
from modules.users.models import Account

from random import choice
from faker import Faker


class Command(BaseCommand):
    def handle(self, *args, **options):
        """
        init fake accounts
        """

        fake = Faker()

        city_lists = City.objects.all()

        # advert
        for account in range(1, 20):
            acc = Account.objects.create_user(
                username=fake.name(),
                email=fake.ascii_safe_email(),
                is_superuser=False,
                phone=fake.phone_number(),
                city=choice(city_lists),
                is_staff=False)

            acc.set_password('1234')
            acc.save()

            print(f'Add fake users #{account}')

        # shops owners
        for account in range(1, 20):
            acc = Account.objects.create_user(
                username=fake.name(),
                email=fake.ascii_safe_email(),
                is_staff=False,
                is_superuser=False,
                phone=fake.phone_number(),
                city=choice(city_lists),
                shop_owner=True,
            )

            acc.set_password('1234')
            acc.save()

            print(f'Add fake users #{account} shops owners')
