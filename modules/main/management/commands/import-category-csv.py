import os
import csv

from django.core.management.base import BaseCommand

from bboard.settings import BASE_DIR
from modules.catalog.models import Category


class Command(BaseCommand):
    def handle(self, *args, **options):

        with open(os.path.join(BASE_DIR, 'data/Category-2020-09-29.csv'), encoding='utf-8') as data:
            for category in csv.DictReader(data, delimiter=',',
                                           fieldnames=(
                                                   'name', 'name_ru', 'name_en', 'name_ka', 'parent', 'icon_name'
                                           )):
                Category.objects.get_or_create(
                    name_ru=category['name_ru'],
                    name_en=category['name_en'],
                    name_ka=category['name_ka'],
                    parent=Category.objects.filter(name=category['parent']).first(),
                    icon_name=category['icon_name']
                )
