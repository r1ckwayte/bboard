from django.core.management.base import BaseCommand

from modules.area.models import City
from modules.users.models import Account
from modules.shops.models import Shop

from random import choice, randint
from faker import Faker


class Command(BaseCommand):
    def handle(self, *args, **options):
        """
        init fake shops
        """

    fake = Faker()

    city_list = City.objects.all()
    account_list = Account.objects.filter(shop_owner=True)

    for shop in range(30):
        Shop.objects.create(
            name=fake.sentence(),
            description="".join(fake.sentence()),
            owner_id=Account.objects.get(id=choice(account_list).id).id,
            address=fake.street_address(),
            city_shop=City.objects.get(id=choice(city_list).id),
            confirmation=True,
            phone=fake.phone_number(),
        )
        print(f"Shop created #{shop}")
