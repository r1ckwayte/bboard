from django.core.management.base import BaseCommand
from modules.area.models import City
from modules.catalog.models import Category
from modules.main.models import Currency, Item
from modules.shops.models import Shop
from modules.users.models import Account

from random import choice, randint
from faker import Faker


class Command(BaseCommand):
    def handle(self, *args, **options):
        """
        init fake advert & products
        """

        fake = Faker()

        city_lists = City.objects.all()
        category_list = Category.objects.all().exclude(parent=None)
        currency_list = Currency.objects.all()

        # fake advert
        account_list = Account.objects.filter(shop_owner=False)

        for product in range(25):
            Item.objects.create(
                author=Account.objects.get(id=choice(account_list).id),
                name="".join(fake.sentence()),
                description="".join(fake.paragraphs(nb=13, ext_word_list=None)),
                category=Category.objects.get(id=choice(category_list).id),
                price=randint(10, 1000000),
                currency=choice(currency_list),
                city_bb=choice(city_lists),
                commercial=False
            )
            print(f'Add fake advert item #{product}!')

        # fake shop products
        shop_list = Shop.objects.all()

        for product in range(25):
            shop = Shop.objects.get(id=choice(shop_list).id)
            Item.objects.create(
                shop=shop,
                author=shop.owner,
                name="".join(fake.sentence()),
                description="".join(fake.paragraphs(nb=13, ext_word_list=None)),
                category=Category.objects.get(id=choice(category_list).id),
                price=randint(10, 1000000),
                currency=choice(currency_list),
                commercial=True
            )
            print(f'Add shop product #{product}!')
