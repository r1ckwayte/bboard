from django.core.management.base import BaseCommand

from modules.area.models import City
from modules.catalog.models import Category
from modules.main.models import Currency
from modules.cms.models import Questions
from modules.orders.models import Status

main_category = [
    {
        'name': "Недвижимость",
        'name_en': "Property",
        'name_ka': "უძრავი ქონება",
        'order': 0,
        'super_rubric': None,
        "icon_name": "icon-home"
    },
    {
        'name': "Электроника",
        'name_en': "Electronics",
        'name_ka': "ელექტრონიკა",
        'order': 1,
        'super_rubric': None,
        "icon_name": "icon-phone"
    },
    {
        'name': "Транспорт",
        'name_en': "Transport",
        'name_ka': "ტრანსპორტი",
        'order': 2,
        'super_rubric': None,
        "icon_name": "icon-car"
    },
    {
        'name': "Бытовая техника",
        'name_en': "Appliances",
        'name_ka': "ტექნიკა",
        'order': 3,
        'super_rubric': None,
        "icon_name": "icon-kitchen"
    },
    {
        'name': "Услуги",
        'name_en': "Services",
        'name_ka': "მომსახურება",
        'order': 4,
        'super_rubric': None,
        "icon_name": "icon-consulting"
    },
    {
        'name': "Работа",
        'name_en': "Job",
        'name_ka': "იმუშავე",
        'order': 5,
        'super_rubric': None,
        "icon_name": "icon-work"
    },
    {
        'name': "Личные вещи",
        'name_en': "Personal items",
        'name_ka': "პირადი ნივთები",
        'order': 6,
        'super_rubric': None,
        "icon_name": "icon-dress"
    },
    {
        'name': "Хобби и отдых",
        'name_en': "Hobbies and leisure",
        'name_ka': "ჰობი და დასვენება",
        'order': 7,
        'super_rubric': None,
        "icon_name": "icon-hobby"
    },
    {
        'name': "Животные",
        'name_en': "Animal",
        'name_ka': "ცხოველები",
        'order': 8,
        'super_rubric': None,
        "icon_name": "icon-zoology"
    },
    {
        'name': "Для бизнеса",
        'name_en': "For business",
        'name_ka': "ბიზნესისთვის",
        'order': 9,
        'super_rubric': None,
        "icon_name": "icon-startup"
    },
    {
        'name': "Медикаменты",
        'name_en': "Medicines",
        'name_ka': "წამალი",
        'order': 10,
        'super_rubric': None,
        "icon_name": "icon-medic"
    },
    {
        'name': "Табачные изделия",
        'name_en': "Tobacco products",
        'name_ka': "თამბაქოს ნაწარმი",
        'order': 11,
        'super_rubric': None,
        "icon_name": "icon-fire"
    },
    {
        'name': "Цветы",
        'name_en': "Flowers",
        'name_ka': "ყვავილები",
        'order': 12,
        'super_rubric': None,
        "icon_name": "icon-flowers"
    }
]

sub_category_list = [
    {
        "name": "Квартиры",
        'name_en': "Flats",
        'name_ka': "ბინები",
        "order": 0,
        "super_rubric_name": 'Недвижимость'
    },
    {
        "name": "Частные дома",
        'name_en': "Private houses",
        'name_ka': "კერძო სახლები",
        "order": 1,
        "super_rubric_name": 'Недвижимость'
    },
    {
        "name": "Земельные участки",
        'name_en': "Land",
        'name_ka': "მიწის ნაკვეთები",
        "order": 4,
        "super_rubric_name": 'Недвижимость'
    },
    {
        "name": "Коммерческая недвижимость",
        'name_en': "Commercial property",
        'name_ka': "კომერციული საკუთრება",
        "order": 5,
        "super_rubric_name": 'Недвижимость'
    },
    {
        "name": "Витамины",
        "name_en": "Vitamins",
        "name_ka": "ვიტამინები",
        "order": 1,
        "super_rubric_name": 'Медикаменты'
    },
    {
        "name": "Лекарства",
        "name_en": "Medication",
        "name_ka": "ტაბლეტები",
        "order": 1,
        "super_rubric_name": 'Медикаменты'
    },
    {
        "name": "Питание",
        "name_en": "Nutrition",
        "name_ka": "კვება",
        "order": 1,
        "super_rubric_name": 'Медикаменты'
    },
    {
        "name": "Прочее",
        "name_en": "Other",
        "name_ka": "სხვა",
        "order": 1,
        "super_rubric_name": 'Медикаменты'
    },
    {
        "name": "Сигареты",
        "name_en": "Cigarettes",
        "name_ka": "სიგარეტი",
        "order": 1,
        "super_rubric_name": 'Табачные изделия'
    },
    {
        "name": "Табак",
        "name_en": "Tobacco",
        "name_ka": "ამბაქო",
        "order": 1,
        "super_rubric_name": 'Табачные изделия'
    },
    {
        "name": "Легковые автомобили",
        'name_en': "Cars",
        'name_ka': "მანქანები",
        "order": 0,
        "super_rubric_name": 'Транспорт'
    },
    {
        "name": "Спецтехника",
        'name_en': "Спецтехника",
        'name_ka': "სპეციალური მანქანები",
        "order": 0,
        "super_rubric_name": 'Транспорт'
    },
    {
        "name": "Грузовики",
        'name_en': "Trucks",
        'name_ka': "სატვირთო მანქანები",
        "order": 0,
        "super_rubric_name": 'Транспорт'
    },
    {
        "name": "Мотоциклы",
        'name_en': "Motorcycles",
        'name_ka': "მოტოციკლები",
        "order": 0,
        "super_rubric_name": 'Транспорт'
    },
    {
        "name": "Запчасти и аксессуары",
        'name_en': "Parts & Accessories",
        'name_ka': "ნაწილები და აქსესუარები",
        "order": 0,
        "super_rubric_name": 'Транспорт'
    },
    {
        "name": "Ремонт и строительство",
        'name_en': "Repair and construction",
        'name_ka': "რემონტი და მშენებლობა",
        "order": 1,
        "super_rubric_name": 'Услуги'
    },
    {
        "name": "Перевозки",
        'name_en': "Transportation",
        'name_ka': "ტრანსპორტირება",
        "order": 2,
        "super_rubric_name": 'Услуги'
    },
    {
        "name": "Ремонт техники",
        'name_en': "Equipment repair",
        'name_ka': "აღჭურვილობის შეკეთება",
        "order": 3,
        "super_rubric_name": 'Услуги'
    },
    {
        "name": "Изготовление на заказ",
        'name_en': "Custom Made",
        'name_ka': "საბაჟო დამზადებულია",
        "order": 4,
        "super_rubric_name": 'Услуги'
    },
    {
        "name": "Красота и здоровье",
        'name_en': "beauty and health",
        'name_ka': "სილამაზე და ჯანმრთელობა",
        "order": 6,
        "super_rubric_name": 'Услуги'
    },
    {
        "name": "Организация мероприятий",
        'name_en': "Organization of events",
        'name_ka': "ღონისძიების ორგანიზება",
        "order": 7,
        "super_rubric_name": 'Услуги'
    },
    {
        "name": "Деловые услуги",
        'name_en': "Business services",
        'name_ka': "საქმიანი მომსახურება",
        "order": 8,
        "super_rubric_name": 'Услуги'
    },
    {
        "name": "Сел/хоз животные",
        'name_en': "Settlement / farm animals",
        'name_ka': "დასახლების/მეურნეობის ცხოველები",
        "order": 4,
        "super_rubric_name": 'Животные'
    },
    {
        "name": "Товары для животных",
        'name_en': "Goods for pets",
        'name_ka': "საოჯახო პროდუქტები",
        "order": 6,
        "super_rubric_name": 'Животные'
    },
    {
        "name": "Другие животные",
        'name_en': "Other animals",
        'name_ka': "სხვა ცხოველები",
        "order": 7,
        "super_rubric_name": 'Животные'
    },
    {
        "name": "Мужская одежда",
        'name_en': "Men's clothing",
        'name_ka': "მამაკაცის ტანსაცმელი",
        "order": 1,
        "super_rubric_name": 'Личные вещи'
    },
    {
        "name": "Женская одежда",
        'name_en': "Women's clothing",
        'name_ka': "ქალის ტანსაცმელი",
        "order": 2,
        "super_rubric_name": 'Личные вещи'
    },
    {
        "name": "Детская одежда",
        'name_en': "Baby clothes",
        'name_ka': "ბავშვის ტანსაცმელი",
        "order": 3,
        "super_rubric_name": 'Личные вещи'
    },
    {
        "name": "Детские товары",
        'name_en': "Childen's goods",
        'name_ka': "ბავშვების საქონელი",
        "order": 3,
        "super_rubric_name": 'Личные вещи'
    },
    {
        "name": "Часы и украшения",
        'name_en': "Watches & Jewelry",
        'name_ka': "საათები და სამკაულები",
        "order": 5,
        "super_rubric_name": 'Личные вещи'
    },
    {
        "name": "Другие товары",
        'name_en': "Other goods",
        'name_ka': "სხვა პროდუქტები",
        "order": 6,
        "super_rubric_name": 'Личные вещи'
    },
    {
        "name": "Музыка и муз. инструменты",
        'name_en': "Music and muses. tools",
        'name_ka': "მუსიკა და მუზა. იარაღები",
        "order": 6,
        "super_rubric_name": 'Хобби и отдых'
    },
    {
        "name": "Настольные игры",
        'name_en': "Board games",
        'name_ka': "სამაგიდო თამაშები",
        "order": 7,
        "super_rubric_name": 'Хобби и отдых'
    },
    {
        "name": "Вакансии",
        'name_en': "Jobs",
        'name_ka': "სამუშაოები",
        "order": 1,
        "super_rubric_name": 'Работа'
    },
    {
        "name": "Резюме",
        'name_en': "Summary",
        'name_ka': "შეჯამება",
        "order": 2,
        "super_rubric_name": 'Работа'
    },
    {
        "name": "Готовый бизнес",
        'name_en': "Ready business",
        'name_ka': "მზა ბიზნესი",
        "order": 1,
        "super_rubric_name": 'Для бизнеса'
    },
    {
        "name": "Оборудование",
        'name_en': "Equipment",
        'name_ka': "აპარატურა",
        "order": 2,
        "super_rubric_name": 'Для бизнеса'
    },
    {
        "name": "Букеты",
        'name_en': "Bouquets",
        'name_ka': "თაიგულები",
        "order": 1,
        "super_rubric_name": 'Цветы'
    },
    {
        "name": "Для дома и сада",
        'name_en': "For home and garden",
        'name_ka': "სახლსა და ბაღში",
        "order": 2,
        "super_rubric_name": 'Цветы'
    },
    {
        "name": "Искуственные",
        'name_en': "Artificials",
        'name_ka': "ხელოვნური",
        "order": 2,
        "super_rubric_name": 'Цветы'
    },
    {
        "name": "Аудио и видео",
        'name_en': "",
        'name_ka': "",
        "order": 0,
        "super_rubric_name": 'Бытовая техника'
    },
    {
        "name": "Игры, приставки и программы",
        'name_en': "",
        'name_ka': "",
        "order": 0,
        "super_rubric_name": 'Бытовая техника'
    },
    {
        "name": "Настольные компьютеры",
        'name_en': "",
        'name_ka': "",
        "order": 0,
        "super_rubric_name": 'Бытовая техника'
    },
    {
        "name": "Оргтехника и расходники",
        'name_en': "",
        'name_ka': "",
        "order": 0,
        "super_rubric_name": 'Бытовая техника'
    },
    {
        "name": "МФУ, копиры и сканеры",
        'name_en': "",
        'name_ka': "",
        "order": 0,
        "super_rubric_name": 'Оргтехника и расходники'
    },
    {
        "name": "Принтеры",
        'name_en': "",
        'name_ka': "",
        "order": 0,
        "super_rubric_name": 'Оргтехника и расходники'
    },
    {
        "name": "Телефония",
        'name_en': "",
        'name_ka': "",
        "order": 0,
        "super_rubric_name": 'Оргтехника и расходники'
    },
    {
        "name": "ИБП, сетевые фильтры",
        'name_en': "",
        'name_ka': "",
        "order": 0,
        "super_rubric_name": 'Оргтехника и расходники'
    },

]
city_list = [
    {'name_ru': "Дедоплисцкаро", 'name_en': "Dedoplistskaro", 'name_ka': "დედოფლისწყარო", },
]

status_list = [
    {"name": "Новый"},
    {"name": "В работе"},
    {"name": "Отменен"},
    {"name": "Выполнен"},
]
list_currency = [
    {'name': 'ლ'},
]
list_question = [
    {'title_ru': "Как подать объявление", 'title_en': "How to place an ad", 'title_ka': "როგორ განათავსოთ განცხადება",
     'content_ru': "Вам нужно авторизоваться. Затем нажать на кнопку 'Подать объявление'",
     'content_en': "You need to log in. Click on the 'Submit Ad' button",
     'content_ka': "თქვენ უნდა შეხვიდეთ სისტემაში. შემდეგ დააჭირეთ ღილაკს 'გაგზავნის განცხადება'",
     'number_ru': "quest-1", 'number_en': "quest-1", 'number_ka': "quest-1"},

    {'title_ru': "Как сменить пароль", 'title_en': "How to change password", 'title_ka': "როგორ შევცვალოთ პაროლი",
     'content_ru': "Сменить пароль можно в личном кабинете",
     'content_en': "You can change the password in your account",
     'content_ka': "შეგიძლიათ შეცვალოთ პაროლი თქვენს ანგარიშში",
     'number_ru': "quest-2", 'number_en': "quest-2", 'number_ka': "quest-2"},

    {'title_ru': "Как разместить свой магазин на сайте", 'title_en': "How to place your store on the site",
     'title_ka': "როგორ მოვათავსოთ თქვენი მაღაზია საიტზე",
     'content_ru': "Вам нужно создать заявку в разделе 'Бизнес'",
     'content_en': "You need to create an application in the 'Business' section",
     'content_ka': "თქვენ უნდა შექმნათ განაცხადი 'ბიზნესის' განყოფილებაში",
     'number_ru': "quest-3", 'number_en': "quest-3", 'number_ka': "quest-3"},

    {'title_ru': "Как обратиться с коммерческим предложением", 'title_en': "How to apply with a commercial offer",
     'title_ka': "როგორ უნდა მიმართოთ კომერციულ შეთავაზებას",
     'content_ru': "В разделе 'Бизнес' вы найдете электронную почту",
     'content_en': "In the 'Business' section you will find an email",
     'content_ka': "'ბიზნესში' განყოფილებაში ნახავთ ელ.წერილს",
     'number_ru': "quest-4", 'number_en': "quest-4", 'number_ka': "quest-4"},

]


class Command(BaseCommand):
    def handle(self, *args, **options):

        for category in main_category:
            if not Category.objects.filter(name=category["name"]):
                Category.objects.create(name=category["name"],
                                        name_en=category["name_en"],
                                        name_ka=category["name_ka"],
                                        icon_name=category["icon_name"]
                                        )
                print(f'category create {category["name"]}')
            else:
                print(f'Категория {category["name"]} уже существует!')

        for sub_cat in sub_category_list:
            if not Category.objects.filter(name=sub_cat["name"]):
                Category.objects.create(name=sub_cat["name"],
                                        name_en=sub_cat["name_en"],
                                        name_ka=sub_cat["name_ka"],
                                        parent=Category.objects.filter(name=sub_cat["super_rubric_name"])[0])

                print(f'add sub category: {sub_cat["name"]}')
            else:
                print(f'Категория {sub_cat["name"]} уже существует!')

        for city in city_list:
            if not City.objects.filter(name_ru=city["name_ru"]):
                City.objects.create(name_ru=city["name_ru"], name_en=city["name_en"], name_ka=city["name_ka"])
                print('add city -  {0}'.format(city))
            else:
                print(f'Категория {city["name_ru"]} уже существует!')

        for status in status_list:
            if not Status.objects.filter(name=status["name"]):
                Status.objects.create(name=status["name"])
                print('add statuc - {0}'.format(status))
            else:
                print(f'Категория {status["name"]} уже существует!')

        for currency in list_currency:
            if not Currency.objects.filter(name=currency["name"]):
                Currency.objects.create(name=currency["name"])
                print('add currency - {0}'.format(currency))
            else:
                print(f'Категория {currency["name"]} уже существует!')

        for question in list_question:
            if not Questions.objects.filter(title_ru=question["title_ru"]):
                Questions.objects.create(title_ru=question["title_ru"], title_en=question["title_en"],
                                         title_ka=question["title_ka"], content_ru=question["content_ru"],
                                         content_en=question["content_en"], content_ka=question["content_ka"],
                                         number_ru=question["number_ru"], number_en=question["number_en"],
                                         number_ka=question["number_ka"])
                print(f'Add question {question["title_ru"]}')
            else:
                print(f'Вопрос {question["title_ru"]} уже существует!')
