from django import template
from django.utils.functional import cached_property

from modules.area.models import City
from modules.catalog.models import Category
# from modules.chat.models import Dialog
from modules.main.models import Item

register = template.Library()


@register.simple_tag()
def total_products():
    return Item.objects.count()


@register.simple_tag()
def category_slider():
    return Category.objects.filter(parent=None).only('id', 'name', 'icon_name')


@register.simple_tag()
def get_category():
    return Category.objects.all()


@register.simple_tag(takes_context=True)
def get_category_name(context):
    category_id = context['request'].path.split("/")[-1]
    return Category.objects.get(id=category_id).name


@cached_property
@register.simple_tag(takes_context=True)
def get_count_category_product(context):
    category_id = context['request'].path.split("/")[-1]

    main_category = Category.objects.get(id=category_id)

    # если, суперкатегория
    if main_category.parent is None:
        # получаем все подкатегории
        sub_category_list = [id.id for id in main_category.children.all().only('id')]

        # получаем список всех продуктов в категориях TODO: count all or is_active=True
        category_count = Item.objects.filter(category_id__in=sub_category_list, is_active=True).count()

    else:
        # для обычной категории возвращаем её товары
        category_count = Item.objects.filter(category_id=category_id, is_active=True).count()

    return category_count


@register.simple_tag()
def get_city_list():
    """Вывод всех категорий"""
    city_list = City.objects.all()

    return city_list


@register.simple_tag(takes_context=True)
def get_current_city(context, page_url=None):
    city_name = City.objects.filter(id=context['request'].path).first()
    return city_name


# @register.simple_tag(takes_context=True)
# def get_unread_count(context):
#     return Dialog.objects.get_unread(context["request"].user).count()
#
#
# @register.simple_tag(takes_context=True)
# def get_shops_unread_count(context):
#     return Dialog.objects.get_shops_unread(context["request"].user).count()