import django_filters

from modules.main.models import Item


class ProductFilter(django_filters.FilterSet):
    price = django_filters.NumberFilter()
    price__gt = django_filters.NumberFilter(field_name='price', lookup_expr='gt')
    price__lt = django_filters.NumberFilter(field_name='price', lookup_expr='lt')

    category = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Item
        fields = ['price', "category"]
