from django.contrib import admin
from django.utils.safestring import mark_safe

from modules.main.models import Item, Image


class ImageInline(admin.TabularInline):
    model = Image
    extra = 0

    readonly_fields = ("get_image",)

    # метод для вывода изображения миниатюр картинок
    def get_image(self, obj):
        return mark_safe(f'<img src={obj.images.url} width="80" height ="60"')

    get_image.short_description = "Изображение"


class ProductImageAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Image._meta.fields]

    class Meta:
        model = Image


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ("id", 'name', 'description', 'author', 'updated_at')
    fields = (('shop', 'author'),
              'name',
              'category',
              'description', 'price', 'currency', 'is_active', 'commercial')
    ordering = ['-updated_at']
    list_filter = ('updated_at', 'commercial')
    inlines = [ImageInline]
    search_fields = ('id', 'name')
