import os
from datetime import datetime

from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.db import models
from django.urls import reverse
from django.utils.functional import cached_property
from phonenumber_field.modelfields import PhoneNumberField

from modules.area.models import City
from modules.catalog.models import Category
from modules.shops.models import Shop
from modules.users.models import Account


class Currency(models.Model):
    name = models.CharField(max_length=10, default='1')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Валюты'
        verbose_name = 'Валюта'


class Item(models.Model):
    category = models.ForeignKey(Category,
                                 on_delete=models.PROTECT,
                                 verbose_name='Категория',
                                 db_index=True,
                                 related_name='items_categories')
    name = models.CharField(max_length=100, verbose_name='Товар')
    shop = models.ForeignKey(Shop, related_name='product_sets',
                             null=True,
                             blank=True,
                             on_delete=models.CASCADE,
                             verbose_name='Магазин')
    short_description = models.TextField(blank=True, null=True, default=None)
    description = models.TextField(verbose_name='Описание')
    price = models.FloatField(default=0, verbose_name='Цена')
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)
    phone = PhoneNumberField(null=True, blank=True, region="GE")
    author = models.ForeignKey(to=Account,
                               related_name='item_sets',
                               on_delete=models.CASCADE,
                               null=True,
                               verbose_name='Автор объявления')
    is_active = models.BooleanField(default=True, db_index=True,
                                    verbose_name='Показывать?')
    created_at = models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Опубликовано')
    updated_at = models.DateTimeField('updated', auto_now=True, db_index=True)
    city_bb = models.ForeignKey(City, verbose_name="Город", on_delete=models.CASCADE, null=True, db_index=True)
    commercial = models.BooleanField(default=False, db_index=True)

    def get_main_image(self):
        # TODO может тут возвращать сразу url главной картинки...

        # если нету главной картинки, выводить 1
        if self.product_image_sets.filter(is_main=True).first():
            main_image = self.product_image_sets.filter(is_main=True).only('id').first()
        else:
            main_image = self.product_image_sets.first()

        return main_image

    @cached_property
    def get_count_images(self):
        return self.product_image_sets.count()

    @cached_property
    def get_all_images(self):
        return self.product_image_sets.all()

    def get_items_all_count(self):
        return self.__class__.objects.get(id=self.id).product_sets.all().count()

    def get_absolute_url(self):
        # return reverse("shops:shop-product", kwargs={"shop_pk": self.shop.id, "product_pk": self.id})
        return reverse('main:product-detail', kwargs={'pk': self.pk})

    def get_author_url(self):
        return reverse('users:account-detail', kwargs={'pk': self.author.id})

    def __str__(self):
        # return f'{self.name} via {self.author.email or ""}'
        return f'{self.name} in {self.city_bb}'

    class Meta:
        verbose_name_plural = 'Item'
        verbose_name = 'Items'
        ordering = ['-updated_at']


def get_product_image_path(instance, filename):
    d = datetime.now()
    print(instance.pk)
    # return os.path.join('shops', str(instance.shop.id), 'images/', filename)
    # FIXME images save path
    return os.path.join('product_images', d.strftime('%Y/%m/%d/'), 'images/', filename)


class Image(models.Model):
    item = models.ForeignKey(to=Item,
                             related_name='product_image_sets',
                             db_index=True,
                             default=None,
                             on_delete=models.CASCADE)

    shop = models.ForeignKey(Shop,
                             blank=True,
                             null=True,
                             default=None,
                             on_delete=models.CASCADE)

    images = models.FileField(
        storage=FileSystemStorage(location=settings.MEDIA_ROOT),
        upload_to='product-images/%Y/%m/%d/',
        verbose_name="Image"
    )  # advert-images/%Y/%m/%d/
    is_main = models.BooleanField(default=False, db_index=True)
    is_active = models.BooleanField(default=True, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated_at = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        verbose_name = 'Image'
        verbose_name_plural = 'Images'

    def __str__(self):
        return f"{self.id}"
