from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.forms import inlineformset_factory
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.views.generic.detail import SingleObjectMixin

from modules.analytics.decorators import counted
from modules.cart.models import Cart
from modules.catalog.models import Category
from modules.main.forms import AddItemForm, EditItemForm, ItemFilterForm
from modules.main.models import Item, Image
from modules.shops.models import Shop
from modules.users.models import City, Account


class CartItemCountMixin(SingleObjectMixin, View):
    object = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.user.is_authenticated:
            user_cart = Cart.objects.filter(user=self.request.user,
                                            is_active=True)
            if user_cart.count() > 0:

                context['cart_items_count'] = user_cart.first().items.count()

            else:
                Cart.objects.create(user=self.request.user)

        return context


class IndexView(ListView, CartItemCountMixin):
    template_name = 'main/index.html'

    def get_queryset(self):
        queryset = []

        if self.kwargs.get("city_pk") is not None:
            queryset = Item.objects.filter(city_bb_id=self.kwargs.get('city_pk')).prefetch_related()

            return queryset

        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # TODO: Add pagination
        context["advert_list"] = Item.objects.select_related(
            'category', 'city_bb', 'currency'
        ).prefetch_related(
            'product_image_sets'
        ).filter(
            is_active=True,
            commercial=False).distinct()[:10]

        context["product_list"] = Item.objects.select_related(
            'city_bb', 'currency',
        ).prefetch_related(
            'product_image_sets', 'shop'
        ).filter(is_active=True, commercial=True)[:10]

        context['shops_list_brand'] = Shop.objects.filter(confirmation=True, product_sets__gte=0).order_by(
            'product_sets')[:10].only('id', 'name', 'brand_image')
        # context["food_list"] = Item.objects.filter(
        # category__in=Category.objects.get(name="Фастфуд").get_children().values('id'))[:10]

        context['popular_item'] = Item.objects.all().select_related(
            'shop', 'currency', 'city_bb'
        ).prefetch_related(
            'product_image_sets',
        ).order_by('-pagehit__count')[:10]

        context["q"] = f'q={self.request.GET.get("q")}&'

        if self.kwargs.get('city_pk'):
            # print(self.kwargs.get('city_pk'))
            context["city_name"] = City.objects.get(id=self.kwargs.get('city_pk'))
            context['item_count'] = Item.objects.filter(city_bb_id=self.kwargs.get('city_pk')).count()

        return context


def filter_facets(facets, qs, form, filters):
    for query_param, filter_param in filters:
        value = form.cleaned_data[query_param]

        if value:
            selected_value = value
            facets['selected'][query_param] = selected_value
            filter_args = {filter_param: value}

            # if 'price__gte' in filter_args:
            #     qs = qs.filter(**filter_args).order_by('price')
            #
            # if 'price__lte' in filter_args:
            #     qs = qs.filter(**filter_args).order_by('price')

            # print('filter_args', filter_args)

            qs = qs.filter(**filter_args).distinct()

    return qs


class ItemList(ListView, CartItemCountMixin):
    template_name = 'main/item-list.html'
    context_object_name = 'product_list'
    paginate_by = 12

    def get_queryset(self):
        # order=asc
        # order=desc

        qs = Item.objects.filter(is_active=True).select_related('currency')

        form = ItemFilterForm(data=self.request.GET)

        facets = {
            "selected": {},
            "categories": {
                # "category": form.fields["category"].queryset,
                "price__gte": form.fields["price__gte"],
                "price__lte": form.fields["price__lte"],
                "commercial": form.fields["commercial"],
            }
        }
        if form.is_valid():
            filters = (
                # ("category", "category"),
                ("price__gte", "price__gte"),
                ("price__lte", "price__lte"),
                ("commercial", 'commercial')
            )
            qs = filter_facets(facets, qs, form, filters)

            return qs

        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # TODO fix count after filter
        context['item_count'] = self.object_list.count()
        context['filter_form'] = ItemFilterForm(self.request.GET)

        return context


class CategoryList(ListView, CartItemCountMixin):
    model = Category
    template_name = 'main/category-list.html'
    context_object_name = 'product_list'
    paginate_by = 12

    def get_queryset(self):

        form = ItemFilterForm(data=self.request.GET)

        facets = {
            "selected": {},
            "categories": {
                # "category": form.fields["category"].queryset,
                "price__gte": form.fields["price__gte"],
                "price__lte": form.fields["price__lte"],
                "commercial": form.fields["commercial"],
            }
        }

        qs = Item.objects.filter(is_active=True).select_related('currency')

        if self.kwargs.get("pk") is not None:

            # проверяем суперкатегория это или нет
            main_category = Category.objects.get(id=self.kwargs.get("pk"))

            # если, суперкатегория
            if main_category.is_root_node():

                # получаем все подкатегории
                sub_category_list = main_category.get_children().only('id')

                # получаем список всех продуктов в категориях
                item_list = Item.objects.filter(category_id__in=sub_category_list, is_active=True).select_related(
                    'city_bb')
            else:
                # для обычной категории возвращаем её товары
                item_list = Item.objects.filter(category__id=self.kwargs.get('pk'), is_active=True)

            if form.is_valid():
                filters = (
                    # ("category", "category"),
                    ("price__gte", "price__gte"),
                    ("price__lte", "price__lte"),
                    ("commercial", 'commercial')
                )
                qs = filter_facets(facets, item_list, form, filters)

                return qs

        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['filter_form'] = ItemFilterForm(self.request.GET)

        return context


@method_decorator(counted, name='dispatch')
class ProductDetail(DetailView, CartItemCountMixin):
    model = Item
    template_name = 'main/item-detail.html'
    context_object_name = 'product'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['account'] = Account.objects.get(id=self.request.user.id)
        # print(Item.objects.get(id=self.kwargs['pk']).shop.product_sets.all().count())
        # Item.objects.filter(commercial=True)[0].shop.product_sets.only('id').count()

        if self.object.commercial:
            item_id = self.object.id
            context['shop'] = Shop.objects.get(id=Item.objects.get(id=item_id).shop_id)

        return context


class FormMixin(object):
    object = None

    def get(self, request, *args, **kwargs):
        if getattr(self, 'is_update_view', False):
            self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset_class = self.get_formset_class()
        formset = self.get_formset(formset_class)
        return self.render_to_response(self.get_context_data(form=form, formset=formset))

    def post(self, request, *args, **kwargs):
        if getattr(self, 'is_update_view', False):
            self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset_class = self.get_formset_class()
        formset = self.get_formset(formset_class)

        if form.is_valid() and formset.is_valid():
            return self.form_valid(form, formset)
        else:
            return self.form_invalid(form, formset)

    def get_formset_class(self):
        return self.formset_class

    def get_formset(self, formset_class):
        return formset_class(**self.get_formset_kwargs())

    def get_formset_kwargs(self):
        kwargs = {
            'instance': self.object
        }
        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs

    def form_valid(self, form, formset):
        form.instance.author = self.request.user
        self.object = form.save()
        formset.instance = self.object
        formset.save()

        if hasattr(self, 'get_success_message'):
            self.get_success_message(form)

        return HttpResponseRedirect(self.object.get_absolute_url())

    def form_invalid(self, form, formset):
        print(form, formset)
        return self.render_to_response(self.get_context_data(form=form, formset=formset))


class ItemCreateView(LoginRequiredMixin, FormMixin, CreateView, CartItemCountMixin):
    model = Item
    form_class = AddItemForm
    formset_class = inlineformset_factory(Item, Image, extra=5, fields=('images',))
    template_name = 'main/item-create.html'
    success_url = '/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['account'] = Account.objects.get(id=self.request.user.id)

        return context


class ItemEditView(LoginRequiredMixin, FormMixin, UpdateView, CartItemCountMixin):
    is_update_view = True
    model = Item
    form_class = EditItemForm
    template_name = 'main/product-edit.html'

    formset_class = inlineformset_factory(Item, Image, extra=5, fields=('images',))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["account"] = Account.objects.get(id=self.request.user.id)

        return context

    def get_object(self, *args, **kwargs):
        obj = super(ItemEditView, self).get_object(*args, **kwargs)

        if not obj.author == self.request.user and not self.request.user.is_superuser:
            raise PermissionDenied
        return obj

    def dispatch(self, *args, **kwargs):
        return super(ItemEditView, self).dispatch(*args, **kwargs)


class ItemDeleteView(LoginRequiredMixin, DeleteView):
    model = Item
    template_name = 'main/product-confirm-delete.html'
    context_object_name = 'product'
    success_url = '/'

    def get_success_url(self):
        return reverse("shops:crud-product", kwargs={'pk': self.kwargs.get("shop_pk")})

    def post(self, request, *args, **kwargs):
        # TODO ajax api
        if Item.objects.get(id=self.kwargs.get('pk')).commercial:
            item_delete = Item.objects.get(id=self.kwargs.get('pk'))
            item_delete.is_active = False
            item_delete.save()

            return HttpResponseRedirect(self.get_success_url())
        else:
            item_delete = Item.objects.get(id=self.kwargs.get('pk'))
            item_delete.is_active = False
            item_delete.save()
            return HttpResponseRedirect(reverse('users:account-detail', kwargs={'pk': self.request.user.id}))


class SearchResultView(ListView, CartItemCountMixin):
    model = Item
    template_name = 'main/search.html'
    context_object_name = 'search_list'

    def get_queryset(self):
        query = self.request.GET.get('q')

        item_list = Item.objects.filter(
            Q(name__icontains=query) | Q(description__icontains=query))

        return item_list
