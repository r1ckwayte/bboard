from django.urls import path
from django.views.decorators.cache import never_cache

from modules.main.views import (
    ProductDetail,
    ItemCreateView,
    ItemEditView,
    ItemDeleteView,
    CategoryList,
    SearchResultView,
    IndexView,
    ItemList,

)

app_name = 'main'

urlpatterns = [
    path('advert/', ItemList.as_view(), name='advert-list'),
    path('product/', ItemList.as_view(), name='product-list'),

    path('product/<int:pk>', ProductDetail.as_view(), name='product-detail'),
    path('product/create', ItemCreateView.as_view(), name='product-create'),
    path('product/edit/<int:pk>', never_cache(ItemEditView.as_view()), name='product-edit'),
    path('product/delete/<int:pk>', ItemDeleteView.as_view(), name='product-delete'),

    # Работа с категориями
    path('category/<int:pk>', CategoryList.as_view(), name='category'),

    path('search/', SearchResultView.as_view(), name='search-result'),

    path('city/<int:city_pk>/', IndexView.as_view(), name='index'),

    path('', IndexView.as_view(), name='index'),
]
