from django.templatetags.static import static
from easy_thumbnails.templatetags.thumbnail import thumbnail_url
from rest_framework import serializers
from rest_framework.fields import FloatField, URLField, ImageField, CharField
from rest_framework.serializers import ModelSerializer

from modules.area.serializers import CitySer

from modules.catalog.serializers import CategorySerializer
from modules.main.models import Item, Currency
from modules.users.serializers import AccountSerializers


class ThumbnailSerializer(serializers.ImageField):
    def __init__(self, alias, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.read_only = True
        self.alias = alias

    def to_representation(self, value):
        if not value:
            return None

        if isinstance(value, str):
            return static(value)

        url = thumbnail_url(value, self.alias)
        return url


class CurrencySerializer(ModelSerializer):
    class Meta:
        model = Currency
        fields = (
            "id",
            "name",
        )


class ProductSerializer(ModelSerializer):
    """Для вывода списка объявлений"""

    # id = HyperlinkedIdentityField(view_name='detail', lookup_field='id')
    url = URLField(source="get_absolute_url", read_only=True)

    price = FloatField(min_value=0, )
    currency = CurrencySerializer()

    author = AccountSerializers()

    city_bb = CitySer()
    image = ImageField(source="featured_image", read_only=True)
    type = CharField(source="__class__.__name__", read_only=True)

    category = CategorySerializer()

    class Meta:
        model = Item
        read_only_fields = ['id']
        fields = (
            "id",
            "url",
            "category",
            "name",
            "description",
            "price",
            "currency",
            "author",
            "created_at",
            "updated_at",
            "city_bb",
            "get_count_images",
            "image",
            "type",
        )
        extra_kwargs = {
            'url': {'lookup_field': 'id'}
        }
