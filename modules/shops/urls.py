from django.urls import path

from modules.shops.views import (
    ShopsList,
    ShopPageView,
    ShopCreateView,
    ShopInfoView,
    ShopProductCreate,
    ShopProductEdit,
    ShopProductDeleteView,
    ShopInfoEdit,
    ShopProductList,
    ShopOrdersListView,
    ShopCreateSuccsessView,
)

app_name = 'shops'

urlpatterns = [
    # список всех магазинов
    path('shops/', ShopsList.as_view(), name='shops-list'),

    # заявка на создание магазина
    path('shop/shop-create/', ShopCreateView.as_view(), name='shop-create'),
    path('shop/shop-create/success', ShopCreateSuccsessView.as_view(), name='shop-create-success'),

    # страница магазина
    path('shop/<int:pk>', ShopPageView.as_view(), name='shop-page'),

    # управление магазином
    path('account/<int:pk>/shops/', ShopInfoView.as_view(), name='shop-info'),
    path('shop/info/<int:pk>/edit', ShopInfoEdit.as_view(), name='shop-info-edit'),

    # список товаров магазина
    path('shop/<int:pk>/product-list', ShopProductList.as_view(), name='crud-product'),

    # создание товара в определенном магазине
    path('shop/<int:shop_pk>/create-product', ShopProductCreate.as_view(), name='create-product'),

    # редактирование товара в определенном магазине
    # path('shop/<int:shop_pk>/product/<int:pk>/edit/', ShopProductEdit.as_view(), name='shop-product-edit'),
    path('shop/<int:shop_pk>/product/<int:pk>/edit/', ShopProductEdit.as_view(), name='product-edit'),
    path('shop/product/<int:pk>/delete/', ShopProductDeleteView.as_view(), name='product-delete'),

    # список заказов
    path('shop/<int:shop_pk>/orders', ShopOrdersListView.as_view(), name='order-list'),
]
