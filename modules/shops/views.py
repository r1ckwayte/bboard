import logging

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.forms import inlineformset_factory
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView, TemplateView

from modules.main.models import Item, Image
from modules.main.views import CartItemCountMixin
from modules.orders.models import Order
from modules.shops.forms import CreateShopForm, ShopFormInfoEdit, ProductImageFormset, ShopProductCreateForm, \
    ShopProductEditForm
from modules.shops.models import Shop
from modules.users.models import Account

logger = logging.getLogger(__name__)


class ShopsList(ListView, CartItemCountMixin):
    """
    Список магазинов
    """

    model = Shop
    context_object_name = 'shop_list'
    template_name = 'shops-list.html'
    queryset = Shop.objects.filter(confirmation=True, product_sets__gte=0).order_by('product_sets') \
        .select_related().prefetch_related('product_sets__currency').defer(
        'address',
        'city_shop',
        'created_at',
        'updated_at')

    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['shops_count'] = Shop.objects.filter(confirmation=True).count()

        # Выборка 7 магазинов по дате редактирования инфы?
        # выборка 12 товаров в каждом магазине

        return context


class ShopCreateView(LoginRequiredMixin, CreateView, CartItemCountMixin):
    """
    Заявка на создание магазина
    """

    form_class = CreateShopForm
    template_name = 'shop-create.html'

    def form_valid(self, form):
        form = form.save(commit=False)
        form.owner = self.request.user
        form.save()

        return HttpResponseRedirect(reverse('shops:shop-create-success'))

    def form_invalid(self, form):
        logger.error(f"{form.errors.items()}")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = CreateShopForm(initial={
            "phone": Account.objects.get(id=self.request.user.id).phone
        })

        return context


class ShopCreateSuccsessView(LoginRequiredMixin, TemplateView, CartItemCountMixin):
    template_name = 'shop-request-create-success.html'


class ShopInfoView(LoginRequiredMixin, DetailView, CartItemCountMixin):
    """
    Управление магазином
    """
    model = Account
    template_name = 'shop-info.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.user.is_authenticated:
            user = Account.objects.get(id=self.request.user.id)

            context['shops'] = user.shop_set.all()

            # FIXME тут может быть ошибка при создании магазина, если у юзера есть права но нету магазинов еще

        return context


class ShopInfoEdit(LoginRequiredMixin, UpdateView):
    model = Shop
    template_name = 'admin/shop-edit.html'
    form_class = ShopFormInfoEdit

    def get_success_url(self):
        return reverse('shops:shop-info-edit', args=[self.kwargs.get('pk')])

    def form_valid(self, form):
        messages.success(self.request, 'Message received 👽')
        return super().form_valid(form)

    # TODO: add loggin invalid form
    def form_invalid(self, form):
        logger.error(f"{form.errors.items()}")

        return messages.error(self.request, form.errors)


class ShopPageView(DetailView, CartItemCountMixin):
    """
    Страница магазина
    """
    model = Shop
    template_name = 'shop-page.html'
    paginate_by = 5
    context_object_name = 'shop'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['product_count'] = Item.objects.filter(shop=self.kwargs.get('pk')).count()

        return context


class ShopProductList(ListView, CartItemCountMixin):
    """
    Список товаров магазина
    """
    template_name = 'admin/crud-product.html'
    context_object_name = 'product_lists'
    paginate_by = 25

    def get_queryset(self):
        return Item.objects.filter(shop=self.kwargs.get("pk"))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['shop'] = Shop.objects.get(id=self.kwargs.get('pk'))
        context['shop_name'] = Shop.objects.get(id=self.kwargs.get('pk')).name
        context['count_all_product'] = Item.objects.filter(shop=self.kwargs.get('pk')).count()

        return context


class ShopProductCreate(LoginRequiredMixin, CreateView):
    """
    Создание продукта в магазине
    """
    model = Item
    form_class = ShopProductCreateForm
    formset_class = inlineformset_factory(Item, Image, extra=5, fields=('item', 'images'))
    template_name = 'admin/create-product.html'
    object = None

    def get_success_url(self):
        return reverse("shops:crud-product", kwargs={'pk': self.kwargs.get("shop_pk")})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['shop'] = Shop.objects.get(id=self.kwargs.get('shop_pk'))
        context['product_image_form'] = ProductImageFormset()

        return context

    def get(self, request, *args, **kwargs):
        form = ShopProductCreateForm(initial={
            "shop": Shop.objects.get(id=self.kwargs.get('shop_pk')),
            "author": self.request.user.id
        })

        return self.render_to_response(
            self.get_context_data(
                form=form,
            ))

    def get_formset_class(self):
        return self.formset_class

    def get_formset(self, formset_class):
        return formset_class(**self.get_formset_kwargs())

    def get_formset_kwargs(self):
        kwargs = {
            'instance': self.object
        }
        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs

    def post(self, request, *args, **kwargs):

        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        formset_class = self.get_formset_class()
        formset = self.get_formset(formset_class)

        if form.is_valid() and formset.is_valid():
            return self.form_valid(form, formset)
        else:
            return self.form_invalid(form, formset)

    def form_valid(self, form, formset):

        self.object = form.save()
        formset.instance = self.object
        formset.save()

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, formset):

        return self.render_to_response(
            self.get_context_data(form=form,
                                  formset=formset,
                                  ))


class ShopProductEdit(LoginRequiredMixin, UpdateView):
    model = Item
    form_class = ShopProductEditForm
    formset_class = inlineformset_factory(Item, Image, extra=5, fields=('item', 'images'))
    context_object_name = 'product'
    template_name = 'admin/edit-product.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['shop'] = Shop.objects.get(id=self.kwargs.get('shop_pk'))
        context['product_image_form'] = ProductImageFormset(instance=self.object)

        return context

    def get_success_url(self):
        return reverse("shops:crud-product", args=[self.kwargs.get("shop_pk")])

    def get(self, request, *args, **kwargs):

        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        product_image_form = ProductImageFormset()

        return self.render_to_response(
            self.get_context_data(
                form=form,
                product_image_form=product_image_form
            ))

    def get_formset_class(self):
        return self.formset_class

    def get_formset(self, formset_class):
        return formset_class(**self.get_formset_kwargs())

    def get_formset_kwargs(self):
        kwargs = {
            'instance': self.object
        }
        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs

    def post(self, request, *args, **kwargs):

        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset_class = self.get_formset_class()
        formset = self.get_formset(formset_class)

        if form.is_valid() and formset.is_valid():
            return self.form_valid(form, formset)
        else:
            return self.form_invalid(form, formset)

    def form_valid(self, form, formset):

        self.object = form.save()
        formset.instance = self.object
        formset.shop = Shop.objects.get(id=self.kwargs.get("shop_pk"))
        formset.save()

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, product_image_form):

        return self.render_to_response(
            self.get_context_data(form=form,
                                  product_image_form=product_image_form,
                                  ))


class ShopProductDeleteView(LoginRequiredMixin, DeleteView):
    model = Item
    # template_name = 'main/product-confirm-delete.html'
    context_object_name = 'product'
    success_url = '/'

    def get_success_url(self):
        return reverse("shops:crud-product", kwargs={'pk': self.kwargs.get("shop_pk")})

    def post(self, request, *args, **kwargs):
        return HttpResponseRedirect(self.get_success_url())


class ShopOrdersListView(LoginRequiredMixin, ListView):
    template_name = 'shop-order-list.html'
    model = Shop

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        shop_id = self.kwargs.get('shop_pk')

        context['shop'] = Shop.objects.get(id=shop_id)

        # TODO list & set to delete duplicate orders
        # TODO filter status order?
        context['orders'] = Order.objects.filter(cart__items__shop__pk=shop_id).distinct()

        return context
