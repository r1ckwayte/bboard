from django.contrib import admin

from modules.shops.models import Shop


@admin.register(Shop)
class ShopAdmin(admin.ModelAdmin):
    list_display = ('name', 'confirmation', 'description', 'owner', 'city_shop')
    list_filter = ('confirmation', 'city_shop')
    search_fields = ("name", )
