import os

from django.db import models
from django.urls import reverse
from phonenumber_field.modelfields import PhoneNumberField

from modules.area.models import City
from modules.users.models import Account, TelegramUser


def get_shop_brand_image_path(instance, filename):
    return os.path.join('avatar_shop', str(instance.id), filename)


class Shop(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название магазина')
    description = models.TextField(blank=True, verbose_name='Описание')  # TODO max_length=200
    owner = models.ForeignKey(Account,
                              blank=False,
                              null=False,
                              on_delete=models.CASCADE,
                              verbose_name='Создатель')
    brand_image = models.ImageField(upload_to=get_shop_brand_image_path,
                                    blank=True, null=True, verbose_name='Логотип магазина',
                                    help_text="Размеры 100х100",
                                    default='img/no-image.png'
                                    )  # TODO уточнить размеры 58x56 100x100 retina display
    confirmation = models.BooleanField(default=False, verbose_name='Подтвержден?')
    employee = models.ManyToManyField(TelegramUser, verbose_name='Сотрудники')
    address = models.CharField(max_length=30, verbose_name='Адрес магазина', blank=True,
                               null=True)  # FIXME: increase max_length
    phone = PhoneNumberField(null=True, blank=True)
    city_shop = models.ForeignKey(City, verbose_name="Город", on_delete=models.CASCADE, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Создано')
    updated_at = models.DateTimeField(auto_now=True, db_index=True, verbose_name='Обновлен')

    def __str__(self):
        return self.name

    def get_all_count_items(self):
        return self.product_sets.count()

    def get_full_name(self):
        return self.name

    def get_avatar(self):
        return self.brand_image if bool(self.brand_image) else "img/blank-profile-picture.png"

    def get_channel_group_name(self):
        return f'chat_shop_{self.id}'

    class Meta:
        verbose_name = 'Магазин'
        verbose_name_plural = 'Магазины'
        ordering = ['-updated_at']

    def get_absolute_url(self):
        return reverse('shops:shop-page', args=(self.id,))
