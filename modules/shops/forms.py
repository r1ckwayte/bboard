from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from django.forms.models import inlineformset_factory

from modules.catalog.models import Category
from modules.main.fields import GroupedModelChoiceField
from modules.shops.models import Shop
from modules.main.models import Item, Image


class CreateShopForm(forms.ModelForm):
    widgets = {'author': forms.HiddenInput()}

    class Meta:
        model = Shop
        fields = ('name', 'description', 'city_shop', 'phone')


class ShopFormInfoEdit(forms.ModelForm):
    class Meta:
        model = Shop
        fields = ('name', 'description', 'brand_image', 'city_shop', 'address', 'phone')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Save info'))


class ShopProductCreateForm(forms.ModelForm):
    category = GroupedModelChoiceField(
        queryset=Category.objects.exclude(parent=None),
        choices_groupby='parent'
    )

    commercial = forms.BooleanField(widget=forms.HiddenInput(), initial=1)

    class Meta:
        model = Item
        fields = ('name', 'category', 'description', 'price', 'currency', 'is_active',
                  'commercial', 'shop', 'author')
        widgets = {'shop': forms.HiddenInput(), 'author': forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['shop'].widget = forms.HiddenInput()
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Add product'))


ProductImageFormset = inlineformset_factory(Item, Image, extra=5, fields=('item', 'images'))


class ShopProductEditForm(forms.ModelForm):
    category = GroupedModelChoiceField(
        queryset=Category.objects.exclude(parent=None),
        choices_groupby='parent'
    )

    class Meta:
        model = Item
        fields = ('name', 'category', 'description', 'price', 'currency', 'is_active', 'shop')
        widgets = {'shop': forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['shop'].widget = forms.HiddenInput()
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Save product'))
