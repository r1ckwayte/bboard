from django_filters import rest_framework as rest_filters, NumberFilter
from rest_framework import filters
from rest_framework import permissions
from rest_framework.pagination import PageNumberPagination
from rest_framework.viewsets import ModelViewSet

from modules.catalog.models import Category
from modules.catalog.serializers import CategorySerializer
from modules.main.models import Item
from modules.main.serializers import ProductSerializer
from modules.users.models import Account
from modules.users.serializers import AccountSerializers


class CategoryApiView(ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.root_nodes()
    pagination_class = None


class Pagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100


class ProductFilter(rest_filters.FilterSet):
    price__gt = NumberFilter(field_name='price', lookup_expr='gt')
    price__lt = NumberFilter(field_name='price', lookup_expr='lt')

    class Meta:
        model = Item
        fields = ["category", "city_bb"]


class ProductApiView(ModelViewSet):
    permission_classes = [permissions.AllowAny]
    queryset = Item.objects.filter(is_active=True)
    serializer_class = ProductSerializer
    pagination_class = Pagination
    filter_backends = (rest_filters.DjangoFilterBackend, filters.SearchFilter)
    filterset_class = ProductFilter
    search_fields = ['title', 'category', 'content']


class AccountApiView(ModelViewSet):
    permission_classes = [permissions.IsAdminUser]
    # permission_classes = [permissions.AllowAny]
    pagination_class = Pagination
    queryset = Account.objects.all()
    serializer_class = AccountSerializers
