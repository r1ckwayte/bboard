from django.urls import path, include
from rest_framework import routers

from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView


# TODO openapi
from modules.api.views import ProductApiView, AccountApiView, CategoryApiView

router = routers.DefaultRouter()

router.register(r'products', ProductApiView)
router.register(r'accounts', AccountApiView)
router.register(r'category', CategoryApiView)

urlpatterns = [
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),

    path('', include(router.urls)),

]
