from django.db import models
from django.utils.functional import cached_property

from modules.main.models import Item


class PageHit(models.Model):
    url = models.CharField(unique=True, max_length=2000)
    count = models.PositiveIntegerField(default=0)
    item = models.ForeignKey(Item, related_name='pagehit', on_delete=models.CASCADE, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.item = Item.objects.get(id=self.url.split('/')[-1])

        super(PageHit, self).save(*args, **kwargs)

    @cached_property
    def get_count_item(self):
        return self.count

    class Meta:
        ordering = ('-count',)
        verbose_name = 'счётчик'
        verbose_name_plural = 'счётчики'

    def __str__(self):
        return f"{self.count} - {self.item.name}"
