from django.contrib import admin

from modules.analytics.models import PageHit


@admin.register(PageHit)
class PageHitAdmin(admin.ModelAdmin):
    list_display = ("id", 'count', 'item', 'url')
