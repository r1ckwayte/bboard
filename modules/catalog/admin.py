from django.contrib import admin

from import_export.admin import ImportExportModelAdmin
from import_export.resources import ModelResource
from mptt.admin import DraggableMPTTAdmin

from modules.catalog.models import Category


class CategoryResource(ModelResource):
    class Meta:
        model = Category
        fields = ('name', 'name_ru', 'name_en', 'name_ka', 'parent', 'icon_name')


@admin.register(Category)
class CategoryImportExportAdmin(ImportExportModelAdmin, DraggableMPTTAdmin):
    resource_class = CategoryResource
    mptt_level_indent = 20
    list_display = ('tree_actions', 'indented_title',)
    list_display_links = ('indented_title',)
