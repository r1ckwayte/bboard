from modeltranslation.translator import register, TranslationOptions

from modules.catalog.models import Category


@register(Category)
class RubricTranslationOptions(TranslationOptions):
    fields = ('name',)
