from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer, Serializer

from modules.catalog.models import Category


class CategorySerializer(ModelSerializer):
    """Для вывода категорий"""

    children = SerializerMethodField('get_children')

    def get_children(self, obj):
        return CategorySerializer(obj.get_children(), many=True).data

    class Meta:
        model = Category
        fields = ('id', 'name', 'children', 'icon_name')
