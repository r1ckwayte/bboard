from django.db import models

from modules.main.models import Item
from modules.users.models import Account


class Favorite(models.Model):
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)

    def __str__(self):
        return f'<{self.user}> - <{self.item}>'
