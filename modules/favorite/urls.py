from django.urls import path

from modules.favorite.views import FavoriteListView, FavoriteLike

app_name = 'favorite'

urlpatterns = [
    path('account/favorite', FavoriteListView.as_view(), name='favorite-list'),
    path('account/favorite-like', FavoriteLike.as_view(), name='favorite-like'),
]