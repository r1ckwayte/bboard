from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.views import View
from django.views.generic import ListView

from modules.favorite.models import Favorite
from modules.main.models import Item
from modules.main.views import CartItemCountMixin
from modules.users.models import Account


class FavoriteListView(ListView, LoginRequiredMixin, CartItemCountMixin):
    template_name = 'favorite-list.html'
    context_object_name = 'favorite_list'

    def get_queryset(self):
        favorite_item = Favorite.objects.filter(user=self.request.user)

        return favorite_item


class FavoriteLike(View, LoginRequiredMixin):

    def post(self, request, *args, **kwargs):

        user_id = int(request.POST.get('user_id'))
        product_id = int(request.POST.get('product_id'))

        # Проверяем есть ли уже в избраном
        check_favorite = Favorite.objects.filter(
            user=Account.objects.get(id=user_id),
            item=Item.objects.get(id=product_id)
        )

        # если есть то удаляем
        if check_favorite.first():

            # print('check_favorite deleted', check_favorite.count(), check_favorite[0].id)

            for fav_item in check_favorite:
                item_id = fav_item.item.id
                fav_item.delete()

                return JsonResponse({'success': True,
                                     "message": "record deleted from favorite",
                                     'item_id': item_id
                                     })

        # если нету добавляем
        else:

            Favorite.objects.create(
                user=Account.objects.get(id=user_id),
                item=Item.objects.get(id=product_id)
            )

            # print("Favorite count", Favorite.objects.filter(user=self.request.user).count())

            return JsonResponse({'success': True,
                                 "message": "record added to favorite",
                                 })
