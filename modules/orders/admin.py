from django.contrib import admin
from django.utils.safestring import mark_safe

from modules.cart.models import Cart
from modules.orders.models import Order, Status
from modules.main.models import Item


@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    list_display = ("id", "name")


class ProductInline(admin.TabularInline):
    model = Item
    extra = 0


class CartInline(admin.TabularInline):
    model = Cart
    extra = 0


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'cart', 'status', 'total_price', 'products_list', 'created_at')
    list_filter = ('status',)

    def products_list(self, obj):
        # each obj will be an Order obj/instance/row
        to_return = '<ul>'
        # I'm assuming that there is a name field under the event.Product model. If not change accordingly.
        to_return += f'\n'.join(
            '<li>{}</li>'.format(prod_name) for prod_name in obj.cart.items.values_list('name', flat=True))
        to_return += '</ul>'
        return mark_safe(to_return)
