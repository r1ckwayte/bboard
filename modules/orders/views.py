from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.views.generic import CreateView

from modules.cart.models import Cart
from modules.orders.forms import OrderCreateForm
from modules.orders.models import Order
from modules.telegram_bot.preparation_for_push import preparation_for_push
from modules.users.models import Account


class OrderCreateView(LoginRequiredMixin, CreateView):
    model = Order
    form_class = OrderCreateForm
    template_name = 'order-create.html'
    success_url = '/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.user.is_authenticated:
            customer = Account.objects.get(id=self.request.user.id)

            form = OrderCreateForm(initial={
                "customer_name": customer.username,
                "customer_email": customer.email,
                "customer_phone": customer.phone,
                "customer_address": customer.address,
            })

            context['form'] = form

        return context

    def form_valid(self, form):
        form = form.save(commit=False)

        user = Account.objects.get(id=self.request.user.id)

        user_cart = Cart.objects.filter(user=user, is_active=True)[0]

        # TODO: check cart sum is not 0
        # TODO: check customer info phone, address

        if user_cart.summ_items_price() != 0:
            form.user = self.request.user
            form.cart = user_cart
            form.total_price = user_cart.summ_items_price()
            form.save()

        # after order created - set cart not active
        user_cart.is_active = False
        user_cart.save()

        for item in user_cart.cart_items():
            # print('тест номера ', user_cart.order_set.all()[0].id)
            # print('user_cart ', user_cart)
            # print('user_cart ', user_cart.id)
            # print("shop_id", item.id)
            # print("item name", item.name)
            # print("shop_name", item.shop)
            # print("employee", item.shop.employee.all())
            # print('type ', type(item.shop.employee.all()))
            # all_tg = item.shop.employee.all()
            # for i in all_tg:
            #     print('i ', i)
            # print('\n')
            number_oder = user_cart.order_set.all()[0].id
            name_item = item.name
            list_employee = item.shop.employee.all()
            preparation_for_push(number_oder, name_item, list_employee)

        # TODO: add redirect to page where info Order, send email or download pdf check

        return HttpResponseRedirect(self.success_url)
