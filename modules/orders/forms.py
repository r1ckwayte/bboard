from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.forms import ModelForm

from modules.orders.models import Order


class OrderCreateForm(ModelForm):
    class Meta:
        model = Order
        fields = (
            'customer_name',
            'customer_email',
            'customer_phone',
            'customer_address',
            'comments',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Сохранить'))
