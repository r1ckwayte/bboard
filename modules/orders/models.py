from django.db import models

from modules.cart.models import Cart
from modules.main.models import Item
from modules.users.models import Account


class Status(models.Model):
    name = models.CharField(max_length=24, blank=True, null=True, default=None)

    # created = models.DateTimeField(auto_now_add=True, auto_now=False)
    # updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    # Новый
    # В работе
    # Отменен
    # Выполнен

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Статус заказа'
        verbose_name_plural = 'Статусы заказа'


class Order(models.Model):
    """
    Заказ
    """
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    total_price = models.DecimalField(max_digits=10, decimal_places=2,
                                      default=0)  # total price for all products in order
    customer_name = models.CharField(max_length=64, blank=True, null=True, default=None)
    customer_email = models.EmailField(blank=True, null=True, default=None)
    customer_phone = models.CharField(max_length=48, blank=True, null=True, default=None)
    customer_address = models.CharField(max_length=128, blank=True, null=True, default=None)
    comments = models.TextField(blank=True, null=True, default=None)

    # shop = models.ForeignKey(Shop, on_delete=models.DO_NOTHING, verbose_name='Магазин')
    # product = models.ForeignKey(Product, verbose_name='Товар', on_delete=models.DO_NOTHING)

    status = models.ForeignKey(Status, default=1, on_delete=models.DO_NOTHING)

    created_at = models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Создано')
    updated_at = models.DateTimeField(auto_now=True, db_index=True, verbose_name='Обновлен')

    def get_product_in_order(self):
        products_id = self.cart.items.values_list('id', flat=True)

        return Item.objects.filter(pk__in=products_id)

    def __str__(self):
        return "Заказ %s %s" % (self.id, self.status.name)

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'
        ordering = ['-updated_at']
