from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from django.contrib.auth.forms import UserCreationForm

from modules.users.models import Account


class RegisterForm(UserCreationForm):
    class Meta:
        model = Account
        fields = ('username',
                  'first_name',
                  'last_name',
                  'phone',
                  'city',
                  'email',
                  'password1',
                  'password2')


class EditAccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = (
            'username',
            'first_name',
            'last_name',
            'email',
            'phone',
            'city',
            'avatar')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Сохранить'))
