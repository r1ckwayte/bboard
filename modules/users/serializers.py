from rest_framework.fields import CharField
from rest_framework.relations import HyperlinkedIdentityField
from rest_framework.serializers import ModelSerializer

from modules.area.serializers import CitySer
from modules.users.models import Account


class AccountSerializers(ModelSerializer):
    id = HyperlinkedIdentityField(view_name='account-detail')
    password = CharField(write_only=True)
    city = CitySer()

    class Meta:
        model = Account
        fields = (
            "id",
            "username",
            'password',
            "email",
            "first_name",
            "last_name",
            "phone",
            "city",
            "avatar"
        )

    def create(self, validated_data):
        user = super(AccountSerializers, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user
