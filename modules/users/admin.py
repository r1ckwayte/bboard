from django.contrib import admin

from modules.main.models import Currency
from modules.users.models import Account, TelegramUser


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Account._meta.fields]

    class Meta:
        model = Account


@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('name', )

    class Meta:
        model = Currency


@admin.register(TelegramUser)
class TelegramUserAdmin(admin.ModelAdmin):
    list_display = ("telegram_id", "account_user", "language")
    list_filter = ('telegram_id', 'language')
