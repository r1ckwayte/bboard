from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse
from phonenumber_field.modelfields import PhoneNumberField

from modules.area.models import City


class Account(AbstractUser):
    """ Custom user model """

    username = models.CharField(
        max_length=200,
        verbose_name='Имя пользователя',
        unique=True, db_index=True)
    email = models.EmailField(verbose_name='Адрес электронной почты',
                              unique=True,
                              null=True,
                              blank=True,
                              db_index=True)
    first_name = models.CharField(max_length=30, null=True, blank=True)
    last_name = models.CharField(max_length=30, null=True, blank=True)
    phone = PhoneNumberField(null=True, blank=True, unique=True, region='GE')
    city = models.ForeignKey(City, on_delete=models.SET_NULL, null=True, blank=True, db_index=True)
    address = models.CharField(max_length=250, null=True, blank=True)
    avatar = models.ImageField(upload_to='avatar/', blank=True, help_text="Avatar size is 48x48")
    courier = models.BooleanField(default=False, blank=True, verbose_name='Доставщик?')
    shop_owner = models.BooleanField(default=False, verbose_name='Владелец магазина')

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def get_items_count_active(self):
        return self.__class__.objects.get(id=self.id).item_sets.all().filter(is_active=True, commercial=False).count()

    def get_items_all_count(self):
        return self.__class__.objects.get(id=self.id).item_sets.all().filter(commercial=False).count()

    def get_items_count_no_active(self):
        return self.__class__.objects.get(id=self.id).item_sets.all().filter(is_active=False, commercial=False).count()

    def get_items_active(self):
        return self.__class__.objects.get(id=self.id).item_sets.filter(is_active=True, commercial=False)

    def get_items_no_active(self):
        return self.__class__.objects.get(id=self.id).item_sets.all().filter(is_active=False, commercial=False)

    def get_account_info(self):
        return self.__class__.objects.get(id=self.id)

    def get_account_city(self):
        return self.__class__.objects.get(id=self.id).city

    def get_full_name(self):
        return "{} {}".format(self.last_name, self.first_name)

    def get_avatar(self):
        return self.avatar if bool(self.avatar) else "img/blank-profile-picture.png"

    def get_channel_group_name(self):
        return f'chat_user_{self.id}'

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return self.username

    def get_absolute_url(self):
        return reverse('users:account-detail', kwargs={'pk': self.pk})


class TelegramUser(models.Model):
    RU = 1
    EN = 2
    GE = 3
    PARAMS = (
        (RU, 'Русский'),
        (EN, 'Английский'),
        (GE, 'Грузинский'),
    )

    telegram_id = models.CharField(max_length=50)
    account_user = models.OneToOneField(Account, on_delete=models.CASCADE, blank=True, null=True)

    language = models.PositiveSmallIntegerField('Язык', choices=PARAMS,
                                                help_text='Язык отображения для юзера',
                                                default=RU, null=True, blank=True)

    def __str__(self):
        return self.telegram_id
