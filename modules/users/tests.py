from django.contrib.sessions.models import Session
from django.test import TestCase, Client
from django.urls import reverse

from modules.users.models import Account


class TestLoginView(TestCase):

    @classmethod
    def setUp(cls):
        cls.password = 'test'
        cls.user = Account(
            username='test',
            email='test@example.com'
        )
        cls.user.set_password(cls.password)
        cls.user.save()
        cls.client = Client()

    def test_incorrect_login(self):
        url = reverse('users:login')
        response = self.client.post(url, {
            'username': self.user.username,
            'password': 'wrong-password',
        }, follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertIn(
            'Please enter a correct username and password',
            str(response.content)
        )
        self.assertEqual(Session.objects.all().count(), 0)

    def test_correct_login(self):
        url = reverse('users:login')
        response = self.client.post(url, {
            'username': self.user.username,
            'password': 'test',
        }, follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Session.objects.all().count(), 1)


# TODO: add tests for correct login
# TODO: add tests for registration

class TestRegisration(TestCase):
    @classmethod
    def setUp(cls):
        cls.client = Client()

    def test_valid(self):
        url = reverse('users:registration')
        username = 'login_test'
        response = self.client.post(url, {
            'username': 'login_test',
            'password1': 'test1234',
            'password2': 'test1234',
        }, follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Account.objects.get(username='login_test'), username)
