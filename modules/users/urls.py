from django.contrib.auth.views import LogoutView, PasswordChangeView, PasswordResetCompleteView, PasswordChangeDoneView, \
    PasswordResetConfirmView, PasswordResetView
from django.urls import path, reverse_lazy

from modules.users.views import (
    ValidateView,
    AccountLoginView,
    RegistrationView,
    AccountDetailView,
    AccountSettingsView,
    HistoryOrderView,
    HistoryOrderDetailView
)

app_name = 'users'

urlpatterns = [
    # вход/выход/регистрация
    path('login', AccountLoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(next_page='/'), name='logout'),

    path('registration', RegistrationView.as_view(), name='registration'),
    path('registration/validate-email/', ValidateView.as_view()),

    path('account/settings/<int:pk>', AccountSettingsView.as_view(), name='settings-profile'),

    # my bb
    path('account/<int:pk>', AccountDetailView.as_view(), name='account-detail'),

    # history orders
    path('account/<int:pk>/orders/', HistoryOrderView.as_view(), name='history-orders'),
    path('account/<int:pk>/orders/<int:order_pk>', HistoryOrderDetailView.as_view(), name='history-detail-orders'),

    # Password reset links (ref: https://github.com/django/django/blob/master/django/contrib/auth/views.py)
    # reset password
    path('password_change/done/',
         PasswordChangeDoneView.as_view(template_name='registration/password-change-done.html'),
         name='password_change_done'),

    path('password_change/',
         PasswordChangeView.as_view(success_url=reverse_lazy('users:password_change_done'),
                                    template_name='registration/password_change.html'),
         name='password_change'),

    path('password_reset/done/',
         PasswordResetCompleteView.as_view(template_name='registration/password_reset_done.html'),
         name='password_reset_done'),

    path('reset/<uidb64>/<token>/', PasswordResetConfirmView.as_view(), name='password_reset_confirm'),

    path('password_reset/',
         PasswordResetView.as_view(success_url=reverse_lazy('users:password_reset_complete')),
         name='password-reset'),

    path('reset/done/',
         PasswordResetCompleteView.as_view(template_name='registration/password_reset_complete.html'),
         name='password_reset_complete'),
]
