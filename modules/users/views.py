from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.http import JsonResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.views import View
from django.views.generic import DetailView, UpdateView, TemplateView

from modules.area.models import City
from modules.main.models import Item
from modules.main.views import CartItemCountMixin
from modules.orders.models import Order
from modules.users.forms import RegisterForm, EditAccountForm
from modules.users.models import Account


class ValidateView(View):

    def get(self, request):
        email = request.GET.get('email')
        username = request.GET.get('username')

        # Поле юзера не пустое, доп проверка
        if username != "":
            check_username = Account.objects.filter(username=username).exists()
            print('check_username', check_username)

        # Поле емейла не пустое
        if email != '':
            try:
                validate_email(email)

                if Account.objects.filter(email__iexact=email).exists():
                    return JsonResponse({
                        "success": False,
                        'error_message': 'A user with this email already exists.'
                    })

            except ValidationError as e:
                return JsonResponse({
                    "success": False,
                    'error_message': 'Email not valid',
                })

        else:
            return JsonResponse({
                "success": False,
                'error_message': 'Email field is empty',
            })

        # если емейл валидный
        return JsonResponse({
            "success": True
        })


# TODO: заблочить показ логин страницы если юзер авторизирован
class RegistrationView(View):

    def get_success_url(self):
        return reverse('main:login')

    def post(self, request, *args, **kwargs):
        registration_form = RegisterForm(request.POST or None)

        # TODO: добавить обработку ошибок в форме, валидация
        # Account.objects.all().order_by('-date_joined')

        if registration_form.is_valid():

            registration_form.save()

            # TODO: нужен ли автологин после регистрации

            # TODO: отправка емейла для подтверждения активации -
            # нужно поле статуса активации в модели Account
            # отправка почты, кеш под хеши ссылок
            # TODO: отправка емейла для подтверждения
            # TODO: не работает сброс пароля

            return JsonResponse({"ok": "You registered!"})

        else:
            return JsonResponse({'success': False,
                                 'errors': [(k, v[0]) for k, v in registration_form.errors.items()]
                                 })


class AccountLoginView(TemplateView):
    template_name = 'login.html'
    form_class = AuthenticationForm

    def post(self, request, *args, **kwargs):

        login_form = AuthenticationForm(data={
            "username": request.POST["username"],
            "password": request.POST["password"]
        })

        if login_form.is_valid():
            username = login_form.cleaned_data.get('username')
            password = login_form.cleaned_data.get('password')

            user = authenticate(username=username, password=password, request=self.request)

            if user is not None:
                login(request, user, backend='django.contrib.auth.backends.ModelBackend')

                return redirect('/')

            else:

                return JsonResponse({'success': False,
                                     'errors': [
                                         {
                                             'some': 'wrong'
                                         },
                                     ]})
        else:
            if request.POST["username"] != "":
                return JsonResponse({'success': False,
                                     'errors': [
                                         {
                                             'username': 'username empty'
                                         },
                                     ]})

            if request.POST["password"] != "":
                return JsonResponse({'success': False,
                                     'errors': [
                                         {
                                             'password': 'password empty'
                                         },
                                     ]})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['city_list'] = City.objects.all()
        return context


class HistoryOrderView(DetailView, LoginRequiredMixin, CartItemCountMixin):
    model = Account
    template_name = 'history-orders.html'
    # TODO add paginate to order table


class HistoryOrderDetailView(DetailView, LoginRequiredMixin, CartItemCountMixin):
    model = Order
    template_name = 'history-detail-orders.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['account'] = Account.objects.get(id=self.request.user.id)

        return context


class AccountDetailView(DetailView, CartItemCountMixin):
    model = Account
    template_name = 'account-detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['account'] = Account.objects.get(id=self.kwargs.get('pk'))

        return context


class AccountSettingsView(LoginRequiredMixin, UpdateView, CartItemCountMixin):
    model = Account
    template_name = 'account-settings.html'
    context_object_name = 'account'
    form_class = EditAccountForm
    slug_url_kwarg = 'name'

    def get_success_url(self):
        return reverse('users:account-detail', args=[self.kwargs.get("pk")])
