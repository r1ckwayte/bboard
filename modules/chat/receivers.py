from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.db.models.signals import post_save
from django.dispatch import receiver

from modules.chat.models import Dialog
from modules.chat.serializers import MessageSerializer


@receiver(post_save, sender='chat.Message')
def update_last_message(sender, instance, created, **kwargs):
    if created:
        instance.dialog.last_message = instance
        instance.dialog.save(update_fields=["last_message"])


@receiver(post_save, sender='chat.Message')
def send_new_message_event(sender, instance, created, **kwargs):
    author = instance.get_author()
    companion = instance.dialog.get_companion(author)

    accounts = [author, companion]

    for account in accounts:
        event = {
            "event_type": "new_message",
            "dialog_id": instance.dialog.id,
            "message": MessageSerializer(instance, context={"account": account}).data,
            "unread_count": Dialog.objects.get_unread(account).count()
        }

        async_to_sync(get_channel_layer().group_send)(
            account.get_channel_group_name(),
            {
                "type": "channels_message",
                "message": event,
            },
        )
