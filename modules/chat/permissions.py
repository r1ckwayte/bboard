from rest_framework import permissions

from modules.chat.models import Dialog


class IsParticipant(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if isinstance(obj, Dialog):
            return obj.has_account(request.user)

        return True
