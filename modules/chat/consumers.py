from asgiref.sync import async_to_sync
from channels.generic.websocket import JsonWebsocketConsumer

from django.utils import timezone
from django.utils.html import escape

from modules.chat.models import Dialog, Message


class ChatConsumer(JsonWebsocketConsumer):
    def connect(self):
        self.user = self.scope["user"]
        self.group_name = self.user.get_channel_group_name()

        async_to_sync(self.channel_layer.group_add)(
            self.group_name,
            self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.group_name,
            self.channel_name
        )

    def channels_message(self, event):
        self.send_json(event["message"])

    def receive_json(self, content, **kwargs):
        if content["type"] == "message":
            self.new_message(content)
        elif content["type"] == "read":
            self.update_read(content)

    def new_message(self, data):
        dialog = Dialog.objects.get(pk=data["dialog_id"])
        if not dialog.has_account(self.user):
            return

        # TODO: Replace to DRF serializer input
        message = Message()
        message.dialog = dialog
        message.out = dialog.get_out_for_account(self.user)
        message.text = escape(data["text"])
        message.date = timezone.now()
        message.is_read = False

        message.save()

    def update_read(self, event):
        dialog = Dialog.objects.get(pk=event["dialog_id"])
        if not dialog.has_account(self.user):
            return

        dialog.set_read_for_account(self.user)
