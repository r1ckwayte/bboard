from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.core.exceptions import SuspiciousOperation
from django.db import models
from django.db.models import Q
from django.urls import reverse
from django.utils import timezone

from modules.shops.models import Shop
from modules.users.models import Account


class DialogManager(models.Manager):
    def get_or_create(self, account_from, account_to, shop_to):
        if account_from is None or (account_to is None and shop_to is None):
            raise SuspiciousOperation('Invalid dialog get_or_create input')

        if account_from == account_to:
            raise SuspiciousOperation('Can`t create dialog with youself')

        if account_to is not None:
            filter_q = (Q(account_from=account_from) & Q(account_to=account_to)) | \
                       (Q(account_from=account_to) & Q(account_to=account_from))
        else:
            filter_q = Q(account_from=account_from) & Q(shop_to=shop_to)

        obj, created = self.filter(filter_q).get_or_create(defaults={
            'account_from': account_from,
            'account_to': account_to,
            'shop_to': shop_to
        })

        return obj

    def get_for_account(self, account):
        return self.filter(Q(account_from=account) | Q(account_to=account)).exclude(last_message__isnull=True)

    def get_unread(self, account):
        if isinstance(account, Account):
            filter_q = (Q(last_message__out=True) & Q(account_to=account)) | \
                       (Q(last_message__out=False) & Q(account_from=account))
        else:
            filter_q = Q(last_message__out=False) & Q(shop_to=account)

        return self.exclude(last_message__isnull=True).filter(filter_q).filter(
            last_message__is_read=False)


class Dialog(models.Model):
    account_from = models.ForeignKey(Account, related_name="dialogs_from_set", null=False, on_delete=models.DO_NOTHING,
                                     verbose_name="Инициатор диалога")
    account_to = models.ForeignKey(Account, related_name="dialogs_to_set", null=True, blank=True,
                                   on_delete=models.DO_NOTHING,
                                   verbose_name="Собеседник-пользователь")
    shop_to = models.ForeignKey(Shop, null=True, blank=True, on_delete=models.DO_NOTHING,
                                verbose_name="Собеседник-магазин")
    last_message = models.ForeignKey('Message', related_name="last_message", null=True, blank=True,
                                     on_delete=models.DO_NOTHING,
                                     verbose_name="Последнее сообщение")

    def get_type(self):
        if self.shop_to is None:
            return "account"
        return "shop"

    def has_account(self, account):
        return self.account_from == account or self.account_to == account

    def get_out_for_account(self, account):
        return account == self.account_from

    def get_companion(self, account):
        if self.get_type() == "account":
            if account == self.account_from:
                return self.account_to
            else:
                return self.account_from
        else:
            if isinstance(account, Shop):
                return self.account_from
            else:
                return self.shop_to

    def set_read_for_account(self, account):
        self.message_set.exclude(out=self.get_out_for_account(account)).update(is_read=True)

        companion = self.get_companion(account)

        event = {
            "event_type": "read_message",
            "dialog_id": self.id
        }

        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            companion.get_channel_group_name(),
            {
                "type": "channels_message",
                "message": event,
            },
        )

    objects = DialogManager()

    class Meta:
        verbose_name = 'Диалог'
        verbose_name_plural = 'Диалоги'
        constraints = [
            models.UniqueConstraint(fields=['account_from', 'account_to'], name='unique user-to-user'),
            models.UniqueConstraint(fields=['account_from', 'shop_to'], name='unique user-to-shop')
        ]

    def get_absolute_url(self):
        return reverse('chat:dialog-detail', kwargs={'pk': self.pk})


class Message(models.Model):
    dialog = models.ForeignKey(Dialog, null=False, on_delete=models.DO_NOTHING, verbose_name="Диалог")
    out = models.BooleanField(verbose_name="Исходящее")
    text = models.TextField(verbose_name="Текст")
    date = models.DateTimeField(default=timezone.now, verbose_name="Отправлено")
    is_read = models.BooleanField(default=False, verbose_name="Прочитано")

    def get_out_for_account(self, account):
        if isinstance(account, Shop):
            return not self.out
        else:
            return self.out if account == self.dialog.account_from else not self.out

    def get_author(self):
        if self.dialog.get_type() == "account":
            return self.dialog.account_from if self.out else self.dialog.account_to
        else:
            return self.dialog.account_from if self.out else self.dialog.shop_to

    class Meta:
        ordering = ["date"]
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
