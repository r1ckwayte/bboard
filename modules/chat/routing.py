from django.urls import re_path

from modules.chat.consumers import ChatConsumer

websocket_urlpatterns = [
    re_path(r'ws/chat/', ChatConsumer),
]
