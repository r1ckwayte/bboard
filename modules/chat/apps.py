from django.apps import AppConfig


class ChatConfig(AppConfig):
    name = 'modules.chat'

    def ready(self):
        pass
