from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import ListView, DetailView, RedirectView
from rest_framework import permissions, authentication
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet

from modules.chat.models import Dialog
from modules.chat.permissions import IsParticipant
from modules.chat.serializers import MessageSerializer, DialogSerializer
from modules.shops.models import Shop
from modules.users.models import Account


class DialogWith(LoginRequiredMixin, RedirectView):
    pattern_name = "chat:dialog-detail"

    def get_redirect_url(self, *args, **kwargs):
        account_from = self.request.user

        account_to_id = self.request.GET.get("account_id", False)
        shop_to_id = self.request.GET.get("shop_id", False)

        account_to = None
        shop_to = None

        if account_to_id:
            account_to = get_object_or_404(Account, pk=account_to_id)
        else:
            shop_to = get_object_or_404(Shop, pk=shop_to_id)

        dialog = Dialog.objects.get_or_create(account_from, account_to, shop_to)

        url = reverse(self.pattern_name, kwargs={"pk": dialog.id})

        args = self.request.META.get('QUERY_STRING', '')
        if args:
            url = "%s?%s" % (url, args)

        return url


class DialogsList(LoginRequiredMixin, ListView):
    model = Dialog
    context_object_name = "dialogs"
    template_name = 'dialogs-list.html'
    paginate_by = 20

    def get_queryset(self):
        return Dialog.objects.get_for_account(self.request.user)


class DialogDetail(LoginRequiredMixin, DetailView):
    model = Dialog
    template_name = 'dialog-detail.html'

    def get_object(self, *args, **kwargs):
        dialog = super(DialogDetail, self).get_object(*args, **kwargs)
        if not dialog.has_account(self.request.user):
            raise PermissionDenied()
        else:
            dialog.set_read_for_account(self.request.user)
            return dialog


class DialogViewSet(ReadOnlyModelViewSet):
    authentication_classes = [authentication.SessionAuthentication]
    permission_classes = [permissions.IsAuthenticated, IsParticipant]

    serializer_class = DialogSerializer

    def get_queryset(self):
        return Dialog.objects.get_for_account(self.request.user)

    @action(detail=True)
    def messages(self, request, pk=None):
        dialog = self.get_object()

        serializer = MessageSerializer(dialog.message_set.all(), many=True, context=self.get_serializer_context())
        return Response(serializer.data)
