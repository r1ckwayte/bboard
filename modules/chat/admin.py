from django.contrib import admin

from modules.chat.models import Dialog, Message


@admin.register(Dialog)
class DialogAdmin(admin.ModelAdmin):
    pass


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    pass
