from rest_framework.fields import CharField, URLField
from rest_framework.serializers import ModelSerializer

from modules.chat.models import Message, Dialog
from modules.main.serializers import ThumbnailSerializer
from modules.shops.models import Shop
from modules.users.models import Account


class ShopSerializer(ModelSerializer):
    avatar_thumbnail = ThumbnailSerializer(alias="avatar", source="get_avatar")

    def to_representation(self, instance):
        ret = super().to_representation(instance)

        ret["type"] = "shop"

        return ret

    class Meta:
        model = Shop
        fields = ['id', 'name', 'avatar_thumbnail']


class AccountSerializer(ModelSerializer):
    name = CharField(source="get_full_name", read_only=True)
    avatar_thumbnail = ThumbnailSerializer(alias="avatar", source="avatar")

    def to_representation(self, instance):
        ret = super().to_representation(instance)

        ret["type"] = "account"

        return ret

    class Meta:
        model = Account
        fields = ['id', 'name', 'username', 'avatar_thumbnail']


class MessageSerializer(ModelSerializer):
    def to_representation(self, instance):
        ret = super().to_representation(instance)

        account = self.context.get("account", False)
        if not account:
            account = self.context.get("request").user

        author = instance.get_author()
        author_serializer = AccountSerializer(author) if isinstance(author, Account) else ShopSerializer(author)

        ret["out"] = instance.get_out_for_account(account)
        ret["author"] = author_serializer.data

        return ret

    class Meta:
        model = Message
        fields = ['id', 'text', 'date', 'is_read']


class DialogSerializer(ModelSerializer):
    url = URLField(source='get_absolute_url', read_only=True)
    last_message = MessageSerializer(read_only=True)

    def to_representation(self, instance):
        ret = super().to_representation(instance)

        ret["type"] = instance.get_type()

        account = self.context["request"].user

        ret["account"] = AccountSerializer(account).data
        if instance.get_type() == "account":
            ret["companion"] = AccountSerializer(instance.get_companion(account)).data
        else:
            ret["companion"] = ShopSerializer(instance.get_companion(account)).data

        return ret

    class Meta:
        model = Dialog
        fields = ['id', 'url', 'last_message']
