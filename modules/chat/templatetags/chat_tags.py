from django import template

register = template.Library()


@register.simple_tag
def get_companion(dialog, account):
    return dialog.get_companion(account)


@register.simple_tag
def is_author(message, account):
    return message.is_author(account)
