from django.urls import path, include
from rest_framework import routers

from modules.chat.views import DialogDetail, DialogViewSet, DialogWith, DialogsList

app_name = 'chat'

router = routers.SimpleRouter()
router.register(r'dialog', DialogViewSet, basename='Dialog')

urlpatterns = [
    path('chat/', DialogsList.as_view(), name='dialogs-list'),
    path('chat/dialog/with/', DialogWith.as_view(), name="dialog-with"),
    path('chat/dialog/<int:pk>', DialogDetail.as_view(), name="dialog-detail"),
    path('chat/api/', include(router.urls))
]
