from django.contrib import admin

from modules.delivery.models import Deliveryman


@admin.register(Deliveryman)
class DeliverymanAdmin(admin.ModelAdmin):
    list_display = ('id', 'deliverman', 'order')
    list_filter = ('taken_at', 'completed_at')
