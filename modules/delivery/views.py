from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.views.generic import ListView, DetailView, TemplateView

from modules.delivery.models import Deliveryman
from modules.orders.models import Order, Status


class DeliverListView(LoginRequiredMixin, ListView):
    model = Order
    template_name = 'delivery.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['active_orders'] = Deliveryman.objects.filter(deliverman=self.request.user,
                                                              order__status__name='В работе')

        return context


class DeliverHistoryListView(LoginRequiredMixin, ListView):
    model = Order
    template_name = 'history.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['active_orders'] = Deliveryman.objects.filter(deliverman=self.request.user,
                                                              order__status__name='Выполнен')

        return context


class OrderListView(LoginRequiredMixin, ListView):
    model = Order
    template_name = 'order-list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['open_orders'] = Order.objects.filter(status__name='Новый')

        return context


class OrderDetailView(LoginRequiredMixin, DetailView):
    model = Order
    template_name = 'order-detail.html'
    context_object_name = 'order'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        order = Order.objects.get(id=self.kwargs.get('pk'))
        # order1 = Order.objects.filter(status__name='В работе')
        if order.status.name != 'Новый':
            context['deliverman'] = Deliveryman.objects.filter(order=order)[0]

        return context


# TODO срок исполения заказ?

class OrderTake(LoginRequiredMixin, TemplateView):

    def post(self, request, pk):

        if request.is_ajax():
            # check deliverman count order
            # добавить проверку, сейчас сохраняются дубликаты... полу пофиксил дисейблом кнопки на жсе
            Deliveryman.objects.create(deliverman=self.request.user, order=Order.objects.get(id=pk))

            order = Order.objects.get(id=pk)
            order.status = Status.objects.filter(name='В работе')[0]
            order.save()

            return JsonResponse({'message': 'success'})
        else:
            return JsonResponse({'message': 'Failed'})


class OrderCancel(LoginRequiredMixin, TemplateView):

    def post(self, request, pk):

        if request.is_ajax():
            order = Order.objects.get(id=pk)
            order.status = Status.objects.filter(name='Новый')[0]
            order.save()
            # TODO нужен ли лог отмен заказов?

            order_deliver = Deliveryman.objects.filter(deliverman=self.request.user, order=order)[0]
            order_deliver.delete()

            return JsonResponse({'message': 'success'})
        else:
            return JsonResponse({'message': 'Failed'})


class OrderDone(LoginRequiredMixin, TemplateView):

    def post(self, request, pk):

        if request.is_ajax():
            # check deliverman count order
            # добавить проверку, сейчас сохраняются дубликаты... полу пофиксил дисейблом кнопки на жсе

            order = Order.objects.get(id=pk)
            if self.request.user == order.deliveryman_set.first().deliverman:
                order.status = Status.objects.filter(name='Выполнен')[0]
                order.save()
                # TODO время выполнения в модель Deliveryman вроде бы проставляется само...
                # order_deliver = Deliveryman.objects.filter(deliverman=self.request.user, order=order)[0]
                # order_deliver.completed_at = datetime.now()
                # order_deliver.save()

            return JsonResponse({'message': 'success'})
        else:
            # TODO: ошибки?
            return JsonResponse({'message': 'Failed'})
