from django.db import models

from modules.orders.models import Order
from modules.users.models import Account


class Deliveryman(models.Model):
    deliverman = models.ForeignKey(
        Account,
        on_delete=models.CASCADE,
        verbose_name='Доставщик'
    )
    order = models.ForeignKey(
        Order,
        on_delete=models.CASCADE,
        verbose_name="Заказ"
    )
    taken_at = models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Взят в исполнение')
    completed_at = models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Выполнен')

    class Meta:
        verbose_name = 'Доставщик'
        verbose_name_plural = 'Доставщики'
        unique_together = ['deliverman', 'order']

    def __str__(self):
        return f"{self.order} - {self.deliverman.username}"
