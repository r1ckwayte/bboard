from django.urls import path

from modules.delivery.views import (DeliverListView,
                                    OrderDetailView,
                                    OrderListView,
                                    DeliverHistoryListView,
                                    OrderTake,
                                    OrderCancel,
                                    OrderDone
                                    )

app_name = 'delivery'

urlpatterns = [
    path('delivery/', DeliverListView.as_view(), name='index'),
    path('delivery/orders', OrderListView.as_view(), name='orders'),
    path('delivery/order-detail/<int:pk>', OrderDetailView.as_view(), name='order-detail'),
    path('delivery/history', DeliverHistoryListView.as_view(), name='history'),
    path('delivery/ordertake/<int:pk>', OrderTake.as_view(), name='order-take'),
    path('delivery/ordercancel/<int:pk>', OrderCancel.as_view(), name='order-cancel'),
    path('delivery/orderdone/<int:pk>', OrderDone.as_view(), name='order-done'),
]
