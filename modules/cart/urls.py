from django.urls import path

from modules.cart.views import CartDetail, CartAdd, CartRemoveProduct, CartClear

app_name = 'cart'

urlpatterns = [
    path('cart/', CartDetail.as_view(), name='cart-detail'),
    path('cart/add/<int:pk>/', CartAdd.as_view(), name='cart-add'),
    path('cart/remove/<int:product_id>/', CartRemoveProduct.as_view(), name='cart-remove'),
    path('cart/cart-clear', CartClear.as_view(), name='cart-clear'),
]
