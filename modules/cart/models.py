from datetime import datetime

from django.contrib.auth.models import User
from django.db import models

from modules.main.models import Item
from modules.users.models import Account


class Cart(models.Model):
    created_at = models.DateTimeField(default=datetime.now)
    user = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="carts")
    items = models.ManyToManyField(Item)
    is_active = models.BooleanField(default=True, db_index=True, verbose_name='cart active')

    TAX_DELIVERY = 2

    def cart_items(self):
        return self.items.all()

    def summ_items_price(self):
        return sum(item.price for item in self.items.all())

    # price cart with tax
    def get_total_sum(self):

        total_sum = 0

        if self.items.all().count() > 0:
            for item in self.items.all():
                total_sum += item.price

        total_sum += self.TAX_DELIVERY

        return total_sum

    class Meta:
        ordering = ['created_at']
        verbose_name = "Корзина"
        verbose_name_plural = "Корзины"

    def __str__(self):
        return f"{self.user.username}"
