from django.contrib import admin

from modules.cart.models import Cart


@admin.register(Cart)
class CartAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'cart_items', 'is_active')
    list_filter = ('is_active', 'created_at')
