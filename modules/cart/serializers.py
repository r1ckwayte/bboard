from rest_framework.serializers import HyperlinkedModelSerializer

from modules.cart.models import Cart
from modules.main.serializers import ProductSerializer


class CartSerializer(HyperlinkedModelSerializer):
    items = ProductSerializer(many=True, read_only=True)

    class Meta:
        model = Cart
        fields = ('url', 'user', 'items',)
