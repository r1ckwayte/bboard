from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse
from django.views import View
from django.views.generic import ListView
from django.views.generic.detail import BaseDetailView

from modules.cart.forms import CartForm
from modules.cart.models import Cart
from modules.main.views import CartItemCountMixin


class CartAdd(LoginRequiredMixin, BaseDetailView):
    model = Cart
    form_class = CartForm

    def get_success_url(self):
        return reverse('main:product-detail', args=[self.kwargs.get("pk")])

    def post(self, request, pk):

        add_cart = CartForm(request.POST)

        if request.is_ajax():
            add_cart.save(commit=False)
            add_cart.user = self.request.user.id
            if add_cart.is_valid():
                # Выбираем корзину юзера
                cart_user = Cart.objects.filter(user=self.request.user, is_active=True)

                # если корзина есть, то добавляем товар в корзину
                if cart_user.count() != 0:

                    cart_user[0].items.add(pk)
                    cart_user[0].save()
                # если нету корзины, создаем и добавляем товар
                else:
                    Cart.objects.create(user=self.request.user)
                    cart_user = Cart.objects.filter(user=self.request.user, is_active=True)

                    cart_user[0].items.add(pk)
                    cart_user[0].save()

            else:
                return JsonResponse({'message': 'form Failed'})
            return JsonResponse({'message': 'success'})
        else:
            return JsonResponse({'message': 'failed'})


class CartDetail(LoginRequiredMixin, ListView, CartItemCountMixin):
    template_name = 'cart-detail.html'
    model = Cart

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        user_cart = Cart.objects.filter(user=self.request.user, is_active=True)

        if user_cart:
            context['cart'] = user_cart[0]

        else:
            Cart.objects.create(user=self.request.user)

        return context


class CartRemoveProduct(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        user_cart = Cart.objects.filter(user=self.request.user, is_active=True)

        if user_cart:
            user_cart[0].items.remove(self.kwargs.get('product_id'))

        return HttpResponseRedirect(reverse('cart:cart-detail'))


class CartClear(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        user_cart = Cart.objects.filter(user=self.request.user, is_active=True)

        if user_cart:
            user_cart[0].items.clear()

        return HttpResponseRedirect(reverse('cart:cart-detail'))
