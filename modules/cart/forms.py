from django import forms

from modules.cart.models import Cart


class CartForm(forms.ModelForm):

    class Meta:
        model = Cart
        fields = ("user",)
