from modeltranslation.translator import register, TranslationOptions
from .models import Questions, Feedback


@register(Questions)
class QuestionsTranslationOptions(TranslationOptions):
    fields = ('title', 'content', 'number')
