from django import forms

from modules.cms.models import Feedback


class AddFeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = ('title', 'content')
