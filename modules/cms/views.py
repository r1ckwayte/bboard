from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, ModelFormMixin

from modules.cms.forms import AddFeedbackForm
from modules.cms.models import Questions, Feedback
import mimetypes


class HelpView(TemplateView):
    template_name = 'cms/help.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['questions'] = Questions.objects.all()
        return context


class LicenseView(TemplateView):
    template_name = 'cms/license.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        file_name = ''
        return context

class AboutView(TemplateView):
    template_name = 'cms/about.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class FeedbackView(CreateView, ModelFormMixin):
    model = Feedback
    form_class = AddFeedbackForm
    template_name = 'cms/feedback.html'
    success_url = '/'

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.save()
        return HttpResponseRedirect(self.success_url)
