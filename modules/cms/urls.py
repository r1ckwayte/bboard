from django.urls import path

from modules.cms import views
from modules.cms.views import AboutView, HelpView, LicenseView, FeedbackView

app_name = 'cms'

urlpatterns = [
    path('about/', AboutView.as_view(), name='about'),
    path('help/', HelpView.as_view(), name='help'),
    path('license/', LicenseView.as_view(), name='license'),
    path('feedback/', FeedbackView.as_view(), name='feedback'),
]
