from django.db import models

from modules.users.models import Account


class Questions(models.Model):
    title = models.CharField(max_length=80, verbose_name='Вопрос')
    content = models.TextField(verbose_name='Ответ')
    number = models.CharField(max_length=10, verbose_name='Порядковый номер')

    class Meta:
        verbose_name_plural = 'Вопросы'
        verbose_name = 'Вопрос'


class Feedback(models.Model):
    author = models.ForeignKey(Account, on_delete=models.CASCADE,
                               verbose_name='Автор обратной связи')
    title = models.CharField(max_length=100, verbose_name='Заголовок')
    content = models.TextField(verbose_name='Описание')

    class Meta:
        verbose_name = 'Обратная связь'
        verbose_name_plural = 'Обратные связи'


