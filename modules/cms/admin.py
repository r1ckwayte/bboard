from django.contrib import admin

from modules.cms.models import Questions, Feedback
from modeltranslation.admin import TranslationAdmin

@admin.register(Questions)
class QuestionsAdmin(TranslationAdmin):
    list_display = ('title', 'content', 'number')


@admin.register(Feedback)
class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('author', 'title', 'content')

