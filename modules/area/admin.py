from django.contrib import admin

from modules.area.models import City
from modeltranslation.admin import TranslationAdmin


@admin.register(City)
class CityAdmin(TranslationAdmin):
    list_display = ("id", "name")
    list_filter = ('name',)
