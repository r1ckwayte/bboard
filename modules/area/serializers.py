from rest_framework import serializers

from modules.area.models import City


class CitySer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = (
            "id",
            "name",
        )
