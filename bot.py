# -*- coding: utf-8 -*-

import os
import django

from modules.telegram_bot import config

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bboard.settings")
django.setup()

import telebot
from telebot import types

from modules.users.models import TelegramUser

bot = telebot.TeleBot(config.token)


# Главное меню
@bot.message_handler(commands=["start"])
def start_bot(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    itembtn1 = types.KeyboardButton('Регистрация')
    itembtn2 = types.KeyboardButton('Помощь')
    markup.add(itembtn1, itembtn2)
    print(message.chat.id)
    bot.send_message(message.chat.id,
                     'Добро пожаловать в компанию "Senca" 😉'
                     '\n'
                     '\nВыберите из меню, что вы хотели бы сделать:'
                     '\n'
                     '\n▶️ Для подключения к боту нажмите - "Регистрация" 😎'
                     '\n▶️ Чтобы узнать подробнее о боте нажмите - "Помощь" 🆘',
                     reply_markup=markup)


@bot.message_handler(commands=["help"])
def help_btn(message):
    markup = types.ReplyKeyboardMarkup(row_width=1)
    registration_btn = types.KeyboardButton('/registration')
    main_btn = types.KeyboardButton('На главную')
    language_btn = types.KeyboardButton('/language')
    markup.add(main_btn, registration_btn, language_btn)

    bot.send_message(message.chat.id,
                     'Данный бот поможет следить владельцам магазинов за продажей товаров. '
                     'В случае покупки товара, бот незамедлительно оповестит об этом'
                     '\nЕсли у Вас остались дополнительные вопросы по боту, напишите нам на почту: kitanin10266@gmail.com'
                     '\n'
                     '\n⚠️ Чтобы начать работать с ним, необходимо пройти регистрацию.⚠️'
                     '\n'
                     '\n▶ ️Для регистрации выполните команду "/registration".😇'
                     '\n▶️ Для смены языка выполните "/language".😜'
                     '\nЧтобы вернуться в главное меню, нажмите соответствующую кнопку'
                     '\n'
                     '\nВыберите из меню, что вы хотели бы сделать:',
                     reply_markup=markup)


def send_push_ru(user_id, message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    registration_btn = types.KeyboardButton('Регистрация')
    main_btn = types.KeyboardButton('Сменить язык')
    markup.add(main_btn, registration_btn)
    bot.send_message(user_id,
                     '{0}'.format(message),
                     reply_markup=markup)


@bot.message_handler(content_types=["text"])
def answer(message):
    if message.text == 'Помощь':
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        registration_btn = types.KeyboardButton('Регистрация')
        main_btn = types.KeyboardButton('/start')
        markup.add(main_btn, registration_btn)

        bot.send_message(message.chat.id,
                         'Данный бот поможет следить владельцам магазинов за продажей товаров. '
                         'В случае покупки товара, бот незамедлительно оповестит об этом'
                         '\n'
                         '\n❗️ Обязательно владелец магазина должен указать ID пользователя в настройках магазина. '
                         'Если указан неверно или вообще не указан, бот работать не будет❗️'
                         '\nДля того чтобы узнать ID напишите боту - @userinfobot'
                         '\n'
                         '\nЕсли у Вас остались дополнительные вопросы, напишите нам на почту: kitanin10266@gmail.com'
                         '\n'
                         '\n⚠️ Чтобы начать работать с ним, необходимо пройти регистрацию ⚠️'
                         '\n'
                         '\n▶ ️Для регистрации введите - Регистрация ".😇'
                         '\n▶️ Вернуться на главную страницу "/start".😜'
                         '\n'
                         '\nВыберите из меню, что вы хотели бы сделать:',
                         reply_markup=markup)

    if message.text == 'На главную':
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        registration_btn = types.KeyboardButton('Регистрация')
        help_bt = types.KeyboardButton('Помощь')
        language_btn = types.KeyboardButton('Сменить язык')
        markup.add(registration_btn, help_bt, language_btn)

        bot.send_message(message.chat.id,
                         'Данный бот поможет следить владельцам магазинов за продажей товаров. '
                         'В случае покупки товара, бот незамедлительно оповестит об этом'
                         '\nЕсли у Вас остались дополнительные вопросы по боту, напишите нам на почту: kitanin10266@gmail.com'
                         '\n'
                         '\n⚠️ Чтобы начать работать с ним, необходимо пройти регистрацию ⚠️'
                         '\n'
                         '\n▶ ️Для регистрации выполните команду "/registration".😇'
                         '\n▶️ Для смены языка выполните "/language".😜'
                         '\nЧтобы вернуться в главное меню, нажмите соответствующую кнопку'
                         '\n'
                         '\nВыберите из меню, что вы хотели бы сделать:',
                         reply_markup=markup)

    if message.text == 'Регистрация':
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        lang_btn = types.KeyboardButton('Сменить язык')
        markup.add(lang_btn)
        user_id = message.chat.id
        print(user_id)

        if TelegramUser.objects.filter(telegram_id=user_id):
            bot.send_message(message.chat.id,
                             'Поздравляем! 😜'
                             '\nВы зарегистрированы.'
                             '\nТеперь Вы будете получать уведомления о покупке товаров'
                             '\n'
                             '\nЕсли хотите сменить язык отображения, нажмите на соответствующую кнопку',
                             reply_markup=markup)
        else:
            bot.send_message(message.chat.id,
                             'Вас нет в базе😱',
                             '\n Попросите своего руководителя добавить вас в качестве сотрудника!😱',
                             reply_markup=markup)

    if message.text == 'Сменить язык':
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        user_id = message.chat.id

        itembtn1 = types.KeyboardButton('Русский')
        itembtn2 = types.KeyboardButton('English')
        itembtn3 = types.KeyboardButton('ქართველი')
        markup.add(itembtn1, itembtn2, itembtn3)

        bot.send_message(message.chat.id,
                         'Выберите язык, на который будет переведен бот:',
                         reply_markup=markup)

    if message.text == 'Русский':
        user_id = message.chat.id
        if TelegramUser.objects.filter(telegram_id=user_id):
            tg_user, _ = TelegramUser.objects.get_or_create(telegram_id=user_id)
            tg_user.language = TelegramUser.RU
            tg_user.save()

            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            main_btn = types.KeyboardButton('На главную')
            markup.add(main_btn)
            bot.send_message(message.chat.id,
                             'Вы успешно сменили язык на Русский',
                             reply_markup=markup)

    if message.text == 'English':
        user_id = message.chat.id
        if TelegramUser.objects.filter(telegram_id=user_id):
            tg_user, _ = TelegramUser.objects.get_or_create(telegram_id=user_id)
            tg_user.language = TelegramUser.EN
            tg_user.save()

            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            main_btn_en = types.KeyboardButton('To main')
            markup.add(main_btn_en)
            bot.send_message(message.chat.id,
                             'You have successfully changed your language to English',
                             reply_markup=markup)

    if message.text == 'ქართველი':
        user_id = message.chat.id
        if TelegramUser.objects.filter(telegram_id=user_id):
            tg_user, _ = TelegramUser.objects.get_or_create(telegram_id=user_id)
            tg_user.language = TelegramUser.GE
            tg_user.save()

            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            main_btn_ge = types.KeyboardButton('მთავარი')
            markup.add(main_btn_ge)
            bot.send_message(message.chat.id,
                             'თქვენ წარმატებით შეცვალეთ ენა ქართულად',
                             reply_markup=markup)

    # На Английском
    if message.text == 'To main':
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        lang_btn = types.KeyboardButton('Change language')
        markup.add(lang_btn)
        user_id = message.chat.id
        print(user_id)

        if TelegramUser.objects.filter(telegram_id=user_id):
            bot.send_message(message.chat.id,
                             'We will notify you how the goods are purchased! 😜'
                             '\nYou can change the language',
                             reply_markup=markup)
        else:
            bot.send_message(message.chat.id,
                             'Вас нет в базе😱',
                             '\n Попросите своего руководителя добавить вас в качестве сотрудника!😱',
                             reply_markup=markup)

    if message.text == 'Change language':
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        itembtn1 = types.KeyboardButton('Русский')
        itembtn2 = types.KeyboardButton('English')
        itembtn3 = types.KeyboardButton('ქართველი')
        markup.add(itembtn1, itembtn2, itembtn3)
        user_id = message.chat.id
        print(user_id)

        if TelegramUser.objects.filter(telegram_id=user_id):
            bot.send_message(message.chat.id,
                             'Select the language you want to change to',
                             reply_markup=markup)
        else:
            bot.send_message(message.chat.id,
                             'Вас нет в базе😱',
                             '\n Попросите своего руководителя добавить вас в качестве сотрудника!😱',
                             reply_markup=markup)

    # На грузинском
    #     смена языка
    if message.text == 'ნის შეცვლა':
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        itembtn1 = types.KeyboardButton('Русский')
        itembtn2 = types.KeyboardButton('English')
        itembtn3 = types.KeyboardButton('ქართველი')
        markup.add(itembtn1, itembtn2, itembtn3)
        user_id = message.chat.id
        print(user_id)

        if TelegramUser.objects.filter(telegram_id=user_id):
            bot.send_message(message.chat.id,
                             'აირჩიეთ ენა, რომლის შეცვლაც გსურთ',
                             reply_markup=markup)
        else:
            bot.send_message(message.chat.id,
                             'Вас нет в базе😱',
                             '\n Попросите своего руководителя добавить вас в качестве сотрудника!😱',
                             reply_markup=markup)
    # на главную
    if message.text == 'მთავარი':
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        lang_btn = types.KeyboardButton('ნის შეცვლა')
        markup.add(lang_btn)
        user_id = message.chat.id
        print(user_id)

        if TelegramUser.objects.filter(telegram_id=user_id):
            bot.send_message(message.chat.id,
                             'ჩვენ გაცნობებთ, თუ როგორ ხდება საქონლის შეძენა! 😜'
                             '\nშეგიძლიათ ენა შეცვალოთ',
                             reply_markup=markup)
        else:
            bot.send_message(message.chat.id,
                             'Вас нет в базе😱',
                             '\n Попросите своего руководителя добавить вас в качестве сотрудника!😱',
                             reply_markup=markup)


def notification_order_ru(number_order, name_item, user_id):
    print('number_order ', number_order)
    print('name_item ', name_item)
    print('user_id ', user_id)
    bot.send_message(user_id,
                     'Оформлен заказ!'
                     '\n'
                     f'\n Заказ под №{number_order}'
                     f'\n Товар: {name_item}'
                     '\n'
                     'Детальную информацию можно посмотреть в панели управления магазином!'
                     )

if __name__ == '__main__':
    print('I started!')
    bot.infinity_polling()
