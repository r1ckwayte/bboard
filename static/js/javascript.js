$(window).on('load', function () {
    $('.home-slider').owlCarousel({
        nav: true,
        dots: false,
        margin: 16,
        responsiveClass: true,
        smartSpeed: 650,
        responsive: {
            0: {
                loop: true,
                center: true,
                items: 1
            },
            320: {
                loop: true,
                center: true,
                items: 1.5
            },
            600: {
                loop: true,
                center: true,
                items: 3
            },
            840: {
                loop: false,
                items: 4
            },
            1180: {
                items: 6,
            }
        }
    });
    $('.slider-index').owlCarousel({
        nav: true,
        dots: false,
        loop: false,
        margin: 16,
        autoplay: true,
        responsiveClass: true,
        smartSpeed: 650,
        autoWidth: true,
        navText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>'],

        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 12,
                nav: true,
                loop: true
            }
        }
    });
    $('.slider-2').owlCarousel({
        nav: true,
        dots: false,
        loop: false,
        margin: 16,
        responsiveClass: true,
        smartSpeed: 650,
        responsive: {
            0: {
                center: true,
                items: 1
            },
            320: {
                center: true,
                items: 2
            },
            660: {
                items: 3
            }
        }
    });
    $('.product-carousel').owlCarousel({
        stagePadding: 40,
        nav: false,
        dots: true,
        loop: false,
        margin: 8,
        smartSpeed: 650,
        autoWidth: true,
        items: 4
    });

    /* Good Job clicks avatars */
    $('.profile__avatar').click(function () {
            $('.profile-menu').addClass('menu__active');
        }
    );

    $(document).on('click', function (e) {
        if (!$(e.target).closest(".profile-menu-dropdown").length) {
            $('.profile-menu').removeClass('menu__active');
        }
        e.stopPropagation();
    });

});