class Notification {
    constructor(WS, account_id) {
        this.WS = WS
        this.account_id = account_id;

        this.WS.addOnMessageHandler(this.onMessage.bind(this));

        this.notificationsPanel = document.getElementById("notifications-panel");
        this.unread_counter = document.getElementById("unread-counter");

        console.log("Notification: Init");
    }

    onMessage(event) {
        let eventData = JSON.parse(event.data);

        switch (eventData.event_type) {
            case "new_message":
                this.updateCounter(eventData);
                this.showNotification(eventData)
                break;
        }
    }

    updateCounter(eventData) {
        this.unread_counter.innerText = eventData["unread_count"];
    }

    showNotification(eventData) {
        const notification = eventData["message"];

        if (notification["out"]) {
            // Dont notify about yours message
            return;
        }

        notification["title"] = "Новое сообщение";
        notification["dialog_id"] = eventData["dialog_id"];
        notification["text"] = truncateText(notification["text"], 150);

        const template = document.getElementById('notification-template').innerHTML;
        const rendered = Mustache.render(template, notification);

        this.notificationsPanel.appendChild(stringToHTML(rendered));

        $($(`.toast[data-notification-id='${notification.id}']`)).toast("show");
    }
}