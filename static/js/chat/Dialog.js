class Dialog {
    constructor(WS, dialog_id, account_id) {
        this.WS = WS;
        this.dialog_id = dialog_id;
        this.account_id = account_id;

        this.WS.addOnOpenHandler(this.onOpen.bind(this));
        this.WS.addOnCloseHandler(this.onClose.bind(this));
        this.WS.addOnMessageHandler(this.onMessage.bind(this));

        this.messagesPanel = document.getElementById("messages-panel");

        this.connectionAlert = document.getElementById("connection-alert");

        this.messageTextInput = document.getElementById("message-text-input");
        this.messageSendButton = document.getElementById("message-send-button");
        this.messageSendButton.onclick = this.onSend.bind(this);

        console.log("Dialog: Init");
    }

    onOpen() {
        this.connectionAlert.classList.add("d-none");
        this.messageSendButton.disabled = false;

        this.loadDialog();
    }

    onClose() {
        this.connectionAlert.classList.remove("d-none");
        this.messageSendButton.disabled = true;
    }

    loadDialog() {
        fetch(`/chat/api/dialog/${this.dialog_id}?format=json`)
            .then(response => response.json())
            .then(dialog => this.initDialog(dialog));
    }

    initDialog(dialog) {
        this.account = dialog["account"];
        this.companion = dialog["companion"];

        fetch(`/chat/api/dialog/${this.dialog_id}/messages?format=json`)
            .then(async response => {
                if (response.ok) {
                    let messages = await response.json();
                    messages.forEach(message => this.addNewMessage(message));
                }
            });
    }

    onMessage(event) {
        let eventData = JSON.parse(event.data);

        if (eventData.dialog_id !== this.dialog_id) {
            // Ignore message no for this dialog
            return;
        }

        switch (eventData.event_type) {
            case "new_message":
                this.addNewMessage(eventData["message"]);
                this.sendRead();
                break;
            case "read_message":
                this.setReadForAllUserMessage();
                break;
        }
    }

    addNewMessage(messageData) {
        if (document.querySelectorAll(`[data-message-id='${messageData.id}']`).length !== 0) {
            console.log("Dialog: Warning: Message already showed");
            return;
        }

        if (!messageData["out"]) {
            messageData["is_read"] = true;
        }

        const template = document.getElementById('message-template').innerHTML;
        const rendered = Mustache.render(template, messageData);

        this.messagesPanel.appendChild(stringToHTML(rendered));

        this.messagesPanel.scrollTop = this.messagesPanel.scrollHeight;
    }

    setReadForAllUserMessage() {
        let messages = document.querySelectorAll(`[data-${this.account.type}-id='${this.account_id}']`);

        messages.forEach(message => {
            message.classList.remove("message-unread");
        });
    }

    sendRead() {
        let readData = {
            type: "read",
            dialog_id: this.dialog_id
        }

        this.WS.send(readData)
    }

    onSend() {
        if (this.messageTextInput.value.length === 0) {
            return;
        }

        let messageData = {
            type: "message",
            dialog_id: this.dialog_id,
            text: this.messageTextInput.value
        }

        this.WS.send(messageData);

        this.messageTextInput.value = "";
    }
}