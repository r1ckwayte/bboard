function stringToHTML(str) {
    const template = document.createElement('template');
    template.innerHTML = str.trim();
    return template.content.firstChild;
}

function truncateText(text, num) {
    if (text.length <= num) {
        return text
    }

    return text.slice(0, num) + "...";
}
