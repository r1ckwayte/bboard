class Dialogs {
    constructor(WS, accountID) {
        this.WS = WS;
        this.accountID = accountID;

        this.WS.addOnOpenHandler(this.onOpen.bind(this));
        this.WS.addOnMessageHandler(this.onMessage.bind(this));

        this.dialogsPanel = document.getElementById("dialogs-panel");

        console.log("Dialogs: Init");
    }

    onOpen() {
        this.loadDialogs();
    }

    loadDialogs() {
        fetch(`/chat/api/dialog/?format=json`)
            .then(response => response.json())
            .then(data => this.initDialogs(data["results"]));
    }

    initDialogs(dialogs) {
        dialogs.forEach(dialog => {
            this.addNewDialog(dialog);
        });
    }

    addNewDialog(dialog) {
        if (document.querySelectorAll(`[data-dialog-id='${dialog.id}']`).length !== 0) {
            console.log("Dialogs: Warning: Dialog already showed");
            return;
        }

        dialog["last_message"]["text"] = truncateText(dialog["last_message"]["text"], 100);

        const template = document.getElementById('dialog-template').innerHTML;
        const rendered = Mustache.render(template, dialog);

        this.dialogsPanel.appendChild(stringToHTML(rendered));
    }

    loadNewDialog(eventData) {
        fetch(`/chat/api/dialog/${eventData.dialog_id}?format=json`)
            .then(response => response.json())
            .then(dialog => this.addNewDialog(dialog));
    }

    onMessage(event) {
        let eventData = JSON.parse(event.data)

        switch (eventData.event_type) {
            case "new_message":
                this.updateDialogs(eventData);
                break;
        }
    }

    updateDialogs(eventData) {
        let dialogs = document.querySelectorAll(`[data-dialog-id='${eventData.dialog_id}']`);

        if (dialogs.length === 0) {
            this.loadNewDialog(eventData);
            return
        }

        this.updateDialog(dialogs[0], eventData["message"])
    }

    updateDialog(dialog, messageData) {
        let messageTextField = dialog.querySelector(".message-text");
        let messageDateField = dialog.querySelector(".message-date");

        messageTextField.innerHTML = truncateText(messageData["text"], 100);
        messageTextField.classList.add("message-unread");

        messageDateField.innerHTML = messageData["date"];
    }
}