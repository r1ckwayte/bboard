class WS {
    constructor() {
        this.available_status = ["CONNECTING", "OPEN", "CLOSING", "CLOSED"];

        let protocol = "ws://";
        if (location.protocol === 'https:') {
            protocol = "wss://";
        }

        this.URL = protocol + window.location.host + '/ws/chat/';
        this.connection = null;

        this.onOpenHandlers = [];
        this.onMessageHandlers = [];
        this.onCloseHandlers = [];
        this.onErrorHandlers = [];

        this.reconnectTimer = null;
    }

    status() {
        return this.available_status[this.connection.readyState];
    }

    addOnOpenHandler(handler) {
        this.onOpenHandlers.push(handler);
    }

    addOnMessageHandler(handler) {
        this.onMessageHandlers.push(handler);
    }

    addOnCloseHandler(handler) {
        this.onCloseHandlers.push(handler);
    }

    addOnErrorHandler(handler) {
        this.onErrorHandlers.push(handler);
    }

    send(obj) {
        this.connection.send(JSON.stringify(obj));
    }

    connect() {
        if (this.connection == null || this.status() === "CLOSED") {
            this.connection = new WebSocket(this.URL);

            this.connection.onopen = this.onopen.bind(this);
            this.connection.onmessage = this.onmessage.bind(this);
            this.connection.onclose = this.onclose.bind(this);
            this.connection.onerror = this.onerror.bind(this);
        } else {
            console.log("WS: Warning: Try connect while current connection alive");
        }
    }

    onopen(event) {
        console.log("WS: OnOpen: " + event);
        console.log(event);

        this.onOpenHandlers.forEach(handler => {
            handler(event);
        });

        if (this.reconnectTimer !== null) {
            clearInterval(this.reconnectTimer);
        }
    }

    onmessage(event) {
        console.log("WS: OnMessage: " + event);
        console.log(event);

        this.onMessageHandlers.forEach(handler => {
            handler(event);
        })
    }

    onclose(event) {
        console.log("WS: OnClose: " + event);
        console.log(event);

        this.onCloseHandlers.forEach(handler => {
            handler(event);
        });

        if (this.reconnectTimer === null) {
            this.reconnectTimer = setInterval(this.connect.bind(this), 5000);
        }
    }

    onerror(error) {
        console.error("WS: OnError: " + error);
        console.log(error);

        this.onCloseHandlers.forEach(handler => {
            handler(error);
        })
    }
}

window.WS = new WS();
window.WS.connect();
