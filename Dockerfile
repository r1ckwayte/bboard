FROM python:3.7

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir /code
COPY . /code/
WORKDIR /code

RUN pip install --upgrade pip
RUN pip install -r requirements/common.txt
