from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('i18n/', include('django.conf.urls.i18n')),
    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.authtoken')),
    path('api/', include('modules.api.urls')),
    path(
        "robots.txt",
        TemplateView.as_view(template_name="robots.txt", content_type="text/plain"),
    ),

]

urlpatterns += i18n_patterns(
    path('', include('modules.main.urls', namespace='main')),
    path('', include('modules.cms.urls', namespace='cms')),
    path('', include('modules.shops.urls', namespace='shops')),
    path('', include('modules.cart.urls', namespace='cart')),
    path('', include('modules.users.urls', namespace='users')),
    path('', include('modules.orders.urls', namespace='orders')),
    path('', include('modules.delivery.urls', namespace='delivery')),
    path('', include('modules.favorite.urls', namespace='favorite')),
    # path('', include('modules.chat.urls', namespace='chat')),
    path('', include('social_django.urls')),
)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_URL)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_URL)

    import debug_toolbar

    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),

        ] + urlpatterns
